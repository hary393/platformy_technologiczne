package ptlab1.v4;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Paths;
import java.nio.file.Path;


public class Ptlab1V4 {

    public static void main(String[] args) {
        Path path = Paths.get("./");
        DiskFile diskFile = null;
        
        diskFile = new DiskFile(path, new SizeComparator());
        //diskFile = new DiskFile(path, new NameLengthComparator());
        //diskFile = new DiskFile(path, null);
        //diskFile = new DiskFile2(path, new SizeComparator());
        
        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("FileDisk.txt"))) {
            oos.writeObject(diskFile);
        }
        catch(IOException ex){
            System.out.println("Blad zapisu/odczytu: " + ex.getMessage());
        }
        
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("FileDisk.txt"))) {
                diskFile = (DiskFile)ois.readObject();
        }
        catch(IOException ex){
            System.out.println("Blad zapisu/odczytu: " + ex.getMessage());
        }
        catch(ClassNotFoundException ex){
             System.out.println("Nie znaleziono klasy: " + ex.getMessage());
        }
        
        System.out.println(diskFile);
    }
    
}
