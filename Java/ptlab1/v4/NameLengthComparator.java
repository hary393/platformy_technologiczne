package ptlab1.v4;

import java.io.Serializable;
import java.util.Comparator;

public class NameLengthComparator implements Comparator<DiskFile>, Serializable{

    @Override
    public int compare(DiskFile o1, DiskFile o2) {
        if(o1.getKindFile() != o2.getKindFile()){
            if(o1.getKindFile() == DiskFile.KindFile.REGULAR_FILE) 
                return -1;
            else 
                return 1;
        }
        else if (o1.getName().length()!= o2.getName().length()){
            if (o1.getName().length() < o2.getName().length())
                return -1;
            else
                return 1;
        }
        else{
            return o1.getName().compareTo(o2.getName());
        }
            
    }
    
}
