package ptlab1.v4;

import java.io.IOException;
import java.io.Serializable;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;


public class DiskFile implements Comparable<DiskFile>, Serializable{
    String name;
    String lastModifiedDate;
    long size;
    KindFile kindFile;
    Set<DiskFile> childFiles;
    Comparator<DiskFile> comparator;
            
    public DiskFile(Path path, Comparator<DiskFile> comparator) {
        name = path.getFileName().toString();
        this.comparator = comparator;
        try{   
            SimpleDateFormat sdf = new SimpleDateFormat("[dd.MM.yy HH:mm]");
            lastModifiedDate = sdf.format(Files.getLastModifiedTime(path).toMillis());
            
            if (Files.isDirectory(path)){
                kindFile = KindFile.DIRECTORY;
                size = 0;
                
                if(comparator != null)
                    childFiles = new TreeSet<>();
                else
                    childFiles = new HashSet<>();
                
                DirectoryStream<Path> stream = Files.newDirectoryStream(path);
                for(Path pathFile : stream){
                    size++;
                    childFiles.add(new DiskFile(pathFile, comparator));
                }
               
            }
            else{
                kindFile = KindFile.REGULAR_FILE;
                size = Files.size(path);
            }
        }
        catch(IOException ex){
            System.out.println("Blad we/wy");
        }
    }

    public DiskFile(){
        
    }
    
    @Override
    public int compareTo(DiskFile o) {
        return comparator.compare(this, o);
    }
    
    public enum KindFile{
        REGULAR_FILE,
        DIRECTORY
    }
    
    public String getName(){
        return name;
    }
    public String getLastModifiedDate(){
        return lastModifiedDate;
    }
    public long getSize(){
        return size;
    }
    public KindFile getKindFile(){
        return kindFile;
    }
    
    @Override
    public String toString(){
        return print(0);
    }
    String print(int poziomZaglebienia){
        String str = new String();
        for (int i=0; i < poziomZaglebienia; i++){
            str += "\t";           
        }
        if (poziomZaglebienia != 0)
            str+= getName();
        else
            str+= ".";
        
        str+= " " + getLastModifiedDate();
        str+= " " + (getKindFile() == KindFile.DIRECTORY ? "K" : "P");
        str+= " (" + String.valueOf(size) + ")";
        if(childFiles != null){
            for(DiskFile diskFile : childFiles){
                str += "\n";
                str += diskFile.print(poziomZaglebienia + 1);
            }
        }
        return str;
    }
    
}
