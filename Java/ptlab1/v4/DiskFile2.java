package ptlab1.v4;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.HashSet;
import java.util.TreeSet;


public class DiskFile2 extends DiskFile{
    
    public DiskFile2(Path path, Comparator<DiskFile> comparator) {
        name = path.getFileName().toString();
        this.comparator = comparator;
        try{   
            SimpleDateFormat sdf = new SimpleDateFormat("[dd.MM.yy HH:mm]");
            lastModifiedDate = sdf.format(Files.getLastModifiedTime(path).toMillis());
            
            if (Files.isDirectory(path)){
                kindFile = KindFile.DIRECTORY;
                size = 0;
                
                if(comparator != null)
                    childFiles = new TreeSet<>();
                else
                    childFiles = new HashSet<>();
                
                DirectoryStream<Path> stream = Files.newDirectoryStream(path);
                for(Path pathFile : stream){
                    size++;
                    childFiles.add(new DiskFile2(pathFile, comparator, true));
                }
               
            }
            else{
                kindFile = KindFile.REGULAR_FILE;
                size = Files.size(path);
            }
        }
        catch(IOException ex){
            System.out.println("Blad we/wy");
        }
    }

    private DiskFile2(Path path, Comparator<DiskFile> comparator, boolean b) {  
        name = path.getFileName().toString();
        this.comparator = comparator;
        try{   
            SimpleDateFormat sdf = new SimpleDateFormat("[dd.MM.yy HH:mm]");
            lastModifiedDate = sdf.format(Files.getLastModifiedTime(path).toMillis());
            
            if (Files.isDirectory(path)){
                kindFile = KindFile.DIRECTORY;
                size = 0;
                
                DirectoryStream<Path> stream = Files.newDirectoryStream(path);
                for(Path pathFile : stream){
                    size++;
                }
            }
            else{
                kindFile = KindFile.REGULAR_FILE;
                size = Files.size(path);
            }
        }
        catch(IOException ex){
            System.out.println("Blad we/wy");
        }
    }
    
}
