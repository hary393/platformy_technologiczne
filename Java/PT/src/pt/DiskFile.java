package pt;



import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.nio.file.Files;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import static java.nio.file.Files.newDirectoryStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class DiskFile implements Serializable{
    public String name;
    public long size;
    public String lastModifiedTime;
    public Rodzaj FileKind;
    public Set<DiskFile> childFiles;
    
    public DiskFile(Path source) throws IOException{ //(Path source, DiskFile Comparator)
        int x=0;
        this.name=source.getFileName().toString();
        List<Path> result = new ArrayList<>();
        if(Files.isDirectory(source)){
            try (DirectoryStream<Path> stream = Files.newDirectoryStream(source)) {
                childFiles = new HashSet<>();
                for (Path entry: stream) {
                    result.add(entry);
                    childFiles.add(new DiskFile(entry));//(Path source, DiskFile Comparator)
                    x=x+1;
                }
       } catch (DirectoryIteratorException ex) {
           // I/O error encounted during the iteration, the cause is an IOException
           throw ex.getCause();
    };}
    if(Files.isDirectory(source)){
        this.FileKind=Rodzaj.Katalog;
        this.size=x;
    }
    else{
        this.FileKind=Rodzaj.Plik;
        this.size=Files.size(source);
    }
        FileTime date = Files.getLastModifiedTime(source);
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        lastModifiedTime =df.format(date.toMillis());
        System.out.println(this.name+this.FileKind+this.lastModifiedTime+this.size);
   }
    public String getName(){
    return this.name;
    }
    public long getSize(){
    return this.size;
    }
    public String getLastModifiedTime(){
    return this.lastModifiedTime;
    }
    public Rodzaj getKind(){
    return this.FileKind;
    }
    public String toString(){
    return "nazwa";
    }
    public String print(String tekst){
        return "";
    }
    public int hashCode(){
        return 1;
    }
    public boolean equals(Object hue){
       return true; 
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
         System.out.println("hue");
         Path source = Paths.get(".\\");
         FileTime date = Files.getLastModifiedTime(source);
         DateFormat df = new SimpleDateFormat("[dd.MM.yy HH:mm]");
         String lastModifiedTime =df.format(date.toMillis());
         System.out.println(lastModifiedTime);
         
         DiskFile hue=new DiskFile(source);
    }
    
    
        public enum Rodzaj {
        Plik,
        Katalog;
    }
}
