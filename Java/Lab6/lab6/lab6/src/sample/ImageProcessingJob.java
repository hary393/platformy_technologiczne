package sample;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;

import java.beans.PropertyChangeSupport;
import java.io.File;

/**
 * Created by Ala on 2015-04-19.
 */
public class ImageProcessingJob {
    private  File file ;
    SimpleStringProperty status;
    SimpleDoubleProperty postep;
    private final PropertyChangeSupport progressHandler = new PropertyChangeSupport(this);



    public PropertyChangeSupport getProgressHandler() {
        return progressHandler;
    }

    public File getFile() {
        return file;
    }

    public File fileProperty() {
        return file;
    }

    public String getStatus() {
        return status.get();
    }

    public SimpleStringProperty statusProperty() {
        return status;
    }

    public void setStatus(String status) {
        this.status.set(status);
    }

    public double getPostep() {
        return postep.get();
    }

    public SimpleDoubleProperty postepProperty() {
        return postep;
    }

    public void setPostep(double postep) {
        double lastProgress = this.postep.get();
        this.postep.set(postep);


        progressHandler.firePropertyChange("postep", lastProgress, postep);

    }

    public ImageProcessingJob(File file,Double d, String s) {
        this.file = file;
        this.postep = new SimpleDoubleProperty(d) ;
        this.status = new SimpleStringProperty(s);
        progressHandler.addPropertyChangeListener("postep", e -> {
            if ((double) e.getNewValue() > 0.0 && (double) e.getNewValue() < 1.0) {
                setStatus("przetwarzanie...");
            } else if ((double) e.getNewValue() >= 1.0) {
                setStatus("zakonczono");
            }
        });
    }

    public ImageProcessingJob() {

    }

}
