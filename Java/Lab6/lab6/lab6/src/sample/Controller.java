package sample;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.*;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.ProgressBarTableCell;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.util.converter.IntegerStringConverter;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Stream;

public class Controller implements Initializable {

    @FXML
    Label miejsceZapisuLabel;
    @FXML
    TableView<ImageProcessingJob> obrazkiTableView;
    @FXML
    TableColumn<ImageProcessingJob,String> nazwaColumn;
    @FXML
    TableColumn<ImageProcessingJob,Double> postepColumn;
    @FXML
    TableColumn<ImageProcessingJob,String> statusColumn;
    @FXML
    TextField idLiczbaWatkow;
    @FXML
    ListView rodzajListView;

    @FXML
    Label czasRow;

    @FXML
    Label czasSek;

    ObservableList<String> observableRodzaje = FXCollections.observableArrayList();

    ObservableList<ImageProcessingJob> observableObrazki = FXCollections.observableArrayList();
    private Path outputDirectory;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ustawKolumny();
        obrazkiTableView.setItems(observableObrazki);
        observableRodzaje.addAll("rownegle","sekwencyjne");
        rodzajListView.setItems(observableRodzaje);
    }

    @FXML
    private void miejsceZapisuAction(ActionEvent event){
        DirectoryChooser dc = new DirectoryChooser();
        File p = dc.showDialog(null);
        if(p!=null) {
            miejsceZapisuLabel.setText(p.getAbsolutePath().toString());
            outputDirectory = p.toPath();
        }
    }

    @FXML
    private void wybierzObrazkiAction(ActionEvent event){
        try {
            List<ImageProcessingJob> selectedFiles = new LinkedList<>();
            FileChooser fc = new FileChooser();
            FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("JPG images", "*.jpg");
            fc.getExtensionFilters().add(filter);

            fc.showOpenMultipleDialog(null).forEach(f -> selectedFiles.add(new ImageProcessingJob(f, 0.0, "oczekiwanie")));


            observableObrazki.addAll(selectedFiles);

        }
        catch(Exception ex){
            System.err.println(ex);
        }
}

    private void ustawKolumny(){
        nazwaColumn.setCellValueFactory(//nazwa pliku
                p-> new SimpleStringProperty(p.getValue().getFile().getName()));
        statusColumn.setCellValueFactory(//status przetwarzania
                p -> p.getValue().statusProperty());
        postepColumn.setCellValueFactory(//postęp przetwarzania
                 p-> p.getValue().postepProperty().asObject());
        postepColumn.setCellFactory(ProgressBarTableCell.<ImageProcessingJob>forTableColumn());
    }

    @FXML
    private void processFilesAction(ActionEvent event){
        try {
            if (rodzajListView.getSelectionModel().getSelectedItem().toString() == "sekwencyjne") {
                new Thread(this::backgroundJob).start();
            }
            else{
                IntegerStringConverter cov = new IntegerStringConverter();
                ForkJoinPool pool = new ForkJoinPool(cov.fromString(idLiczbaWatkow.getText()));
                pool.submit(this::backgroundJobParallel);
            }
        }
        catch(Exception ex){
            System.err.println(ex);
        }
    }

    private void backgroundJob(){
        long start = System.currentTimeMillis();

        Stream<ImageProcessingJob> s;
            s = obrazkiTableView.getItems().stream();
            s.forEach(ipj -> {
                if (ipj.getStatus() != "zakonczono") {
                    convertToGrayscale(ipj.getFile(), outputDirectory.toFile(), ipj);
                }
            });



        long end = System.currentTimeMillis();//czas po zakończeniu operacji
        long duration = end - start;//czas przetwarzania
       
        Platform.runLater(() -> czasSek.setText(String.valueOf(System.currentTimeMillis() - start)+"ms"));


    }

    private void backgroundJobParallel(){
        long start = System.currentTimeMillis();

        Stream<ImageProcessingJob> s;
        s = obrazkiTableView.getItems().parallelStream();
        s.forEach(ipj -> {
            if(ipj.getStatus()!="zakonczono"){
                convertToGrayscale(ipj.getFile(),outputDirectory.toFile(),ipj);
            }
        });

        long end = System.currentTimeMillis();//czas po zakończeniu operacji
        long duration = end - start;//czas przetwarzania
        Platform.runLater(() -> czasRow.setText(String.valueOf(System.currentTimeMillis() - start)));


    }

    private void convertToGrayscale(File originalFile, File outputDir, ImageProcessingJob job) {
        try {
            BufferedImage original = ImageIO.read(originalFile);
            BufferedImage greyscale = new BufferedImage(original.getWidth(), original.getHeight(), original.getType());

            for (int i = 0; i < original.getWidth(); i++) {
                for (int j = 0; j < original.getHeight(); j++) {
                    Color color = new Color(original.getRGB(i, j));
                    int luminosity = (int) (0.21 * color.getRed() + 0.71 * color.getGreen() + 0.07 * color.getBlue());
                    int newPixel = new Color(luminosity, luminosity, luminosity).getRGB();
                    greyscale.setRGB(i, j, newPixel);
                }

                double progress = (1.0 + i) / original.getWidth();
                Platform.runLater(() -> job.setPostep(progress));
            }

            Path outputPath = Paths.get(outputDir.getAbsolutePath(),originalFile.getName());
            ImageIO.write(greyscale, "jpg", outputPath.toFile());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @FXML
    private void defaultAction(ActionEvent event){
        miejsceZapisuLabel.setText("D:\\temp");
        idLiczbaWatkow.textProperty().setValue("2");
        rodzajListView.getSelectionModel().selectLast();
    }

    @FXML
    private void usunAction(ActionEvent event){
        observableObrazki.clear();
    }
}

