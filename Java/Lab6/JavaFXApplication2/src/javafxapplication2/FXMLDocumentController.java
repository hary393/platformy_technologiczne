/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication2;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Stream;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.ProgressBarTableCell;
import javafx.scene.layout.AnchorPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.util.converter.IntegerStringConverter;
import javax.imageio.ImageIO;
import javafxapplication2.Obraz;
        
/**
 *
 * @author student
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    TableView<Obraz> obrazTableView;
    @FXML
    TableColumn<Obraz,String> imageNameColumn;
    @FXML
    TableColumn<Obraz,Double> progressColumn;
    @FXML
    TableColumn<Obraz,String> statusColumn;
    @FXML
    ListView rodzajListView;
    @FXML
    Label czas;
    @FXML
    RadioButton radio1;        
    @FXML
    RadioButton radio2;    
    @FXML
    ToggleGroup myToggleGroup;
    @FXML
    AnchorPane Pane;


    ObservableList<Obraz> observableObrazy = FXCollections.observableArrayList();
    private Path outputDirectory;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        imageNameColumn.setCellValueFactory(//nazwa pliku
            p-> new SimpleStringProperty(p.getValue().getFile().getName()));
        statusColumn.setCellValueFactory(//status przetwarzania
            p -> p.getValue().statusProperty());
        progressColumn.setCellValueFactory(//postęp przetwarzania
            p-> p.getValue().progressProperty().asObject());
        progressColumn.setCellFactory(ProgressBarTableCell.<Obraz>forTableColumn());
        obrazTableView.setItems(observableObrazy);


    }

    
    
    
    @FXML
    private void zapiszDoAction(ActionEvent event){
        DirectoryChooser dc = new DirectoryChooser();
        File p = dc.showDialog(null);
        if(p!=null) {
            outputDirectory = p.toPath();
        }
    }

    @FXML
    private void wybierzObrazAction(ActionEvent event){
        try {
            List<Obraz> selectedFiles = new LinkedList<>();
            FileChooser fc = new FileChooser();
            FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("JPG images", "*.jpg");
            fc.getExtensionFilters().add(filter);

            fc.showOpenMultipleDialog(null).forEach(f -> selectedFiles.add(new Obraz(f, 0.0, "czekaj")));


            observableObrazy.addAll(selectedFiles);

        }
        catch(Exception ex){
            System.err.println(ex);
        }
}


    @FXML
    private void zacznijAction(ActionEvent event){
        String hue=((RadioButton) myToggleGroup.getSelectedToggle()).getText();
        
        try {
            if ("Równolegle".equals(hue)) {
               ForkJoinPool pool = new ForkJoinPool(2);
               pool.submit(this::backgroundJobParallel);
            }
            else{
                new Thread(this::backgroundJob).start();
            }
        }
        catch(Exception ex){
            System.err.println(ex);
        }
    }

    private void backgroundJob(){
        long start = System.currentTimeMillis();

        Stream<Obraz> s;
            s = obrazTableView.getItems().stream();
            s.forEach(ipj -> {
                if (ipj.getStatus() != "koniec") {
                    convertToGrayscale(ipj.getFile(), outputDirectory.toFile(), ipj);
                }
            });



        long end = System.currentTimeMillis();//czas po zakończeniu operacji
        long duration = end - start;//czas przetwarzania
       
        Platform.runLater(() -> czas.setText(String.valueOf(System.currentTimeMillis() - start)+"ms"));


    }

    private void backgroundJobParallel(){
        long start = System.currentTimeMillis();

        Stream<Obraz> s;
        s = obrazTableView.getItems().parallelStream();
        s.forEach(ipj -> {
            if(ipj.getStatus()!="koniec"){
                convertToGrayscale(ipj.getFile(),outputDirectory.toFile(),ipj);
            }
        });

        long end = System.currentTimeMillis();//czas po zakończeniu operacji
        long duration = end - start;//czas przetwarzania
        Platform.runLater(() -> czas.setText(String.valueOf(System.currentTimeMillis() - start)+"ms"));


    }

    private void convertToGrayscale(File originalFile, File outputDir, Obraz job) {
        try {
            BufferedImage original = ImageIO.read(originalFile);
            BufferedImage greyscale = new BufferedImage(original.getWidth(), original.getHeight(), original.getType());

            for (int i = 0; i < original.getWidth(); i++) {
                for (int j = 0; j < original.getHeight(); j++) {
                    Color color = new Color(original.getRGB(i, j));
                    int luminosity = (int) (0.21 * color.getRed() + 0.71 * color.getGreen() + 0.07 * color.getBlue());
                    int newPixel = new Color(luminosity, luminosity, luminosity).getRGB();
                    greyscale.setRGB(i, j, newPixel);
                }

                double progress = (1.0 + i) / original.getWidth();
                Platform.runLater(() -> job.setProgress(progress));
            }

            Path outputPath = Paths.get(outputDir.getAbsolutePath(),originalFile.getName());
            ImageIO.write(greyscale, "jpg", outputPath.toFile());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
