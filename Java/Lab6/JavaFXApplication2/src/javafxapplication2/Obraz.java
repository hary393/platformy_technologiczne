/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication2;

import java.beans.PropertyChangeSupport;
import java.io.File;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author student
 */
public class Obraz {
     private  File file ;
    SimpleStringProperty status;
    DoubleProperty progress;
    private final PropertyChangeSupport progressHandler = new PropertyChangeSupport(this);



    public PropertyChangeSupport getProgressHandler() {
        return progressHandler;
    }

    
    public String getStatus() {
        return status.get();
    }
    
    public void setStatus(String status) {
        this.status.set(status);
    }
    
    public SimpleStringProperty statusProperty() {
        return status;
    }
    
    public File getFile() {
        return file;
    }

    public File fileProperty() {
        return file;
    }


    public double getProgress() {
        return progress.get();
    }

    public DoubleProperty progressProperty() {
        return progress;
    }

    public void setProgress(double postep) {
        double lastProgress = this.progress.get();
        this.progress.set(postep);


        progressHandler.firePropertyChange("postep", lastProgress, postep);

    }

    public Obraz(File file,Double d, String s) {
        this.file = file;
        this.progress = new SimpleDoubleProperty(d) ;
        this.status = new SimpleStringProperty(s);
        progressHandler.addPropertyChangeListener("postep", lis -> {
            if ((double) lis.getNewValue() > 0.0 && (double) lis.getNewValue() < 1.0) {
                setStatus("trwa");
            } else if ((double) lis.getNewValue() >= 1.0) {
                setStatus("koniec");
            }
        });
    }
    
}
