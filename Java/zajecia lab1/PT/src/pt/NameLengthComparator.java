/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt;

import static com.sun.org.apache.xml.internal.security.keys.keyresolver.KeyResolver.length;
import java.io.Serializable;
import java.util.Comparator;
import pt.DiskFile.Rodzaj;

/**
 *
 * @author student
 */
public class NameLengthComparator implements  Comparator<DiskFile>, Serializable {

    @Override
    public int compare(DiskFile pierwszy, DiskFile drugi){
        if(pierwszy.getKind()!= drugi.getKind()){
            if(pierwszy.getKind()== Rodzaj.Plik)return -1;
            if(drugi.getKind()== Rodzaj.Plik)return 1;
        }
        if(pierwszy.getName().length()!= drugi.getName().length()){
            if(pierwszy.getName().length()> drugi.getName().length()) return-1;
            if(pierwszy.getName().length()< drugi.getName().length()) return 1;
        }
        return pierwszy.getName().compareTo(drugi.getName());
        
    }


    
}

