package pt;



import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.nio.file.Files;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;


public class DiskFile implements Comparable<DiskFile>, Serializable{
    public String name;
    public long size;
    public String lastModifiedTime;
    public Rodzaj FileKind;
    public Set<DiskFile> childFiles;
    
    public DiskFile(Path source,Comparator<DiskFile> hue) throws IOException{ 
        int x=0;
        this.name=source.getFileName().toString();
        if(Files.isDirectory(source)){
            try (DirectoryStream<Path> stream = Files.newDirectoryStream(source)) {
                childFiles = new TreeSet<>();
                for (Path entry: stream) {
                    childFiles.add(new DiskFile(entry,hue));
                    x=x+1;
                }
       } catch (DirectoryIteratorException ex) {
           // I/O error encounted during the iteration, the cause is an IOException
           throw ex.getCause();
            };
        }
    if(Files.isDirectory(source)){
        this.FileKind=Rodzaj.Katalog;
        this.size=x;
    }
    else{
        this.FileKind=Rodzaj.Plik;
        this.size=Files.size(source);
    }
        FileTime date = Files.getLastModifiedTime(source);
        DateFormat df = new SimpleDateFormat("[dd.MM.yy HH:mm]");
        lastModifiedTime =df.format(date.toMillis());
   }
    public String getName(){
    return this.name;
    }
    public long getSize(){
    return this.size;
    }
    public String getLastModifiedTime(){
    return this.lastModifiedTime;
    }
    public Rodzaj getKind(){
    return this.FileKind;
    }
    public String toString(){
    return (this.name+" "+this.lastModifiedTime+" "+this.FileKind+" "+this.size);
    }
    
    public String print(String tekst){
        String Result =tekst+this.toString()+"\n";
        if(this.FileKind==Rodzaj.Katalog){
            for(DiskFile F: childFiles){
            Result+=F.print(tekst+"\t");
            }
        }
        return Result;
    }

    public int compareTo(DiskFile cos){
        if(FileKind==Rodzaj.Plik){
        return -1;
        }
        else if (cos.FileKind==Rodzaj.Plik){
        return 1;
        }
        else{
            if(getSize()>cos.getSize()){
                return 1;
            }
            else if(getSize()<cos.getSize()){
                return -1;
            }
            else{
                return 0;
            }
        }
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + Objects.hashCode(this.name);
        hash = 47 * hash + (int) (this.size ^ (this.size >>> 32));
        hash = 47 * hash + Objects.hashCode(this.FileKind);
        return hash;
    }
    
    public boolean equals(Object hue){
        if(this==hue){
            return true; 
        }
        DiskFile drugi=(DiskFile) hue;
        if(name==drugi.getName() && FileKind==drugi.getKind() && lastModifiedTime==drugi.getLastModifiedTime()
                && size==drugi.getSize()){
        return true;
        }
        else {
            return false;
        }
    }
    
    
    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException, ClassNotFoundException {
         Path source = Paths.get(".\\");
         DiskFile hue=new DiskFile(source, (Comparator<DiskFile>) new SizeComparator());
     
        System.out.println(hue.print(""));
        
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("\\notatnik"));
        oos.writeObject(hue);
        
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("\\notatnik"))) {
            DiskFile seria = (DiskFile) ois.readObject();
            System.out.println(seria.print("")); 
        }
          
        
        try (BufferedWriter bw = new BufferedWriter(new FileWriter("\\tekst.txt"))) {
                bw.write(hue.print(""));
        }
        try (BufferedReader br = new BufferedReader(new FileReader("\\tekst.txt"))) {
            String line = null;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
        }
    }
    
        public enum Rodzaj {
        Plik,
        Katalog;
    }
}
