/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt;

import java.io.Serializable;
import java.util.Comparator;
import pt.DiskFile.Rodzaj;

/**
 *
 * @author student
 */
public class SizeComparator implements  Comparator<DiskFile>, Serializable {

    @Override
    public int compare(DiskFile pierwszy, DiskFile drugi){
        if(pierwszy.getKind()!= drugi.getKind()){
            if(pierwszy.getKind()== Rodzaj.Plik)return -1;
            if(drugi.getKind()== Rodzaj.Plik)return 1;
        }
        if(pierwszy.getSize() < drugi.getSize()) return -1;
        else if(pierwszy.getSize() > drugi.getSize()) return 1;
        return pierwszy.getName().compareTo(drugi.getName());
        
    }


    
}
