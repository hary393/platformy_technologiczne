/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author PC10
 */
public class MultiServerThread implements Runnable {
    Socket socket=null;
    
    MultiServerThread(Socket socket) {
    this.socket=socket;
    }
    
    @Override
    public void run(){
        try {
            InputStream in = socket.getInputStream();  
           
            DataInputStream clientData = new DataInputStream(in);   

            String fileName = clientData.readUTF();     
            OutputStream output = new FileOutputStream(fileName);     
            byte[] buffor = new byte[1024];
            int dataSize;
            while ((dataSize = clientData.read(buffor)) > -1) {
                output.write(buffor, 0, buffor.length);
            }
            output.close();
            System.out.println("Przesłano plik "+fileName);
            in.close();
            clientData.close();
            
        } catch (IOException ex) {
            Logger.getLogger(MultiServerThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
