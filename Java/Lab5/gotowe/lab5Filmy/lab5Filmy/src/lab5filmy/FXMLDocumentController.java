/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5filmy;

import java.net.URL;
import java.util.Collection;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.converter.IntegerStringConverter;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author Ala
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private TableView <Kategoria> kategoriaTableView;
    @FXML
    private TableView filmTableView;
    
    @FXML
    private TableColumn <Film,String> rezyserColumn;
    @FXML
    private TableColumn <Film,String> tytulColumn;
    @FXML
    TextField nazwaTextField; 
    @FXML
    TextField tytulTextField; 
    @FXML
    TextField id2TextField; 
    @FXML
    TextField rezyserTextField; 
    @FXML
    TextField rokWydaniaTextField; 
    @FXML
    TextField idTextField; 
    @FXML
    private TableColumn <Kategoria,String> titleColumn;
    private static final Logger log = Logger.getLogger(FXMLDocumentController.class.getName());

    private ObservableList<Kategoria> kategorie = FXCollections.observableArrayList();
    ListProperty <Film> filmy = new SimpleListProperty<>(); 
    private EntityManager em;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
         em = Persistence.createEntityManagerFactory("lab5FilmyPU").createEntityManager();
         go_with_film();
         go_with_kategoria(); 
    }    

    private void go_with_kategoria() {
        kategoriaTableView.setItems(kategorie);
        titleColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        titleColumn.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<Kategoria, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<Kategoria, String> t) {
                t.getRowValue().setNazwa(t.getNewValue());
                update(t.getRowValue());
            }
        });
        for(int i =0; i<kategorie.size();i++){
            System.out.println(kategorie.get(i).getNazwa());
        }
        kategoriaTableView.setEditable(true);
        List<Kategoria> dbbook = em.createNamedQuery("Kategoria.findAll").getResultList();
        kategorie.addAll(dbbook); 
    }
     private void update(Kategoria b ){
        try{
            em.getTransaction().begin(); 
            em.merge(b); 
            em.getTransaction().commit(); 
        }
        catch(Exception ex){
            if(em.getTransaction().isActive()){
                em.getTransaction().rollback(); 
            }
        }finally{
            
        }
    }
     
    private void update2(Film b ){
        try{
            em.getTransaction().begin(); 
            em.merge(b); 
            em.getTransaction().commit(); 
        }
        catch(Exception ex){
            if(em.getTransaction().isActive()){
                em.getTransaction().rollback(); 
            }
        }finally{
            
        }
    }

    private void go_with_film() {
        tytulColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        tytulColumn.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<Film, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<Film, String> t) {
                t.getRowValue().setTytul(t.getNewValue());
                update2(t.getRowValue());
            }
        });
        rezyserColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        rezyserColumn.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<Film, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<Film, String> t) {
                t.getRowValue().setRezyser(t.getNewValue());
                update2(t.getRowValue());
            }
        });
        kategoriaTableView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Kategoria>() {
            @Override
            public void changed(ObservableValue<? extends Kategoria> value, Kategoria oldValue, Kategoria newV) {
                //final ObservableList items = filmTableView.getItems();
                    
             //   filmy.clear();
                if(newV!=null){
                    if (newV != null) {
                        filmy.set(FXCollections.observableList(newV.getFilmList()));
                    } else {
                        filmy.setValue(null);
                    }   
                }
            }
        });
        filmTableView.itemsProperty().bind(filmy);
        filmTableView.setEditable(true);
        
    }
    @FXML
    private void addAction(ActionEvent event){
        try{
            String ti = nazwaTextField.textProperty().getValue().toString();
            String pa = idTextField.textProperty().getValue().toString();
            IntegerStringConverter conv = new IntegerStringConverter();
            Kategoria b = new Kategoria(conv.fromString(pa)); 
            b.setNazwa(ti);
            //b.setFilmList(filmy);
            //b.setAuthorCollection(filmy);
            persist(b); 
            kategorie.add(b);
        }
        catch (Exception ex){
            System.err.println(ex); 
        }
    }
    @FXML
    public void delAction(ActionEvent e){
        try{
            Kategoria selected = (Kategoria) kategoriaTableView.getSelectionModel().getSelectedItem();  
            if (selected != null && selected.getFilmList()==null){
                remove(selected);
                kategorie.remove(selected);
            }
        }
        catch (Exception ex){
            System.err.println(ex); 
        }
    } 
   
    @FXML 
    private void add2Action(ActionEvent e){
       try{
        Kategoria selected = (Kategoria) kategoriaTableView.getSelectionModel().getSelectedItem(); 
        Integer ki = selected.getIdKat();
        String ti = rokWydaniaTextField.textProperty().getValue();
        String ty = tytulTextField.textProperty().getValue();
        String re = rezyserTextField.textProperty().getValue();
        String id = id2TextField.textProperty().getValue();
        IntegerStringConverter conv = new IntegerStringConverter();
        Film b = new Film(conv.fromString(id)); 
        b.setTytul(ty);
        b.setRezyser(re);
        b.setRokWydania(conv.fromString(ti));
        b.setIdKat(selected);


         //b.setFilmList(filmy);
         //b.setAuthorCollection(filmy);
         persist2(b); 
         filmy.add(b);
       }
       catch(Exception ex){
           System.err.println(ex); 
       }
       
    }
    public void delete2Action(ActionEvent e){
        try{
            Film selected = (Film) filmTableView.getSelectionModel().getSelectedItem();  
            if (selected != null){
                remove2(selected);
                filmy.remove(selected);
            }
        }
        catch (Exception ex){
            System.err.println(ex); 
        }
    }
        
    private void remove(Kategoria b) {
        try {
            em.getTransaction().begin();
            em.remove(em.merge(b));
            em.getTransaction().commit();
        } catch (Exception ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
        } finally {
        }
    }
    private void remove2(Film b) {
        try {
            em.getTransaction().begin();
            em.remove(em.merge(b));
            em.getTransaction().commit();
        } catch (Exception ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
        } finally {
        }
    }
    
    private void persist(Kategoria b) {
        try {
            em.getTransaction().begin();
            em.persist(b);
            em.getTransaction().commit();
        } catch (Exception ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
        } finally {
        }
    }
    private void persist2(Film b) {
        try {
            em.getTransaction().begin();
            em.persist(b);
            em.getTransaction().commit();
        } catch (Exception ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
        } finally {
        }
    }
}
