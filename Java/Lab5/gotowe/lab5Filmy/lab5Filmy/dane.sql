
 
CREATE Table Kategoria
 (id_kat INTEGER PRIMARY KEY,
  nazwa CHAR(30)
); 
CREATE Table Film 
 (id INTEGER PRIMARY KEY,
  tytul CHAR(30),
  rezyser CHAR(30),
  rok_wydania INTEGER,
  id_kat INTEGER, 
  FOREIGN KEY(id_kat) REFERENCES Kategoria(id_kat)
  
 );

INSERT INTO Kategoria (id_kat,nazwa) VALUES (1,'horror');
INSERT INTO Kategoria (id_kat,nazwa) VALUES (2,'komedia');
INSERT INTO Kategoria (id_kat,nazwa) VALUES (3,'komedia romantyczna');
INSERT INTO Kategoria (id_kat,nazwa) VALUES (4,'kryminal');
INSERT INTO Kategoria (id_kat,nazwa) VALUES (5,'bajka');

INSERT INTO Film(id,tytul,rezyser,rok_wydania,id_kat) VALUES (1,'Film1','Kowalski',2000,2);
INSERT INTO Film(id,tytul,rezyser,rok_wydania,id_kat) VALUES (2,'Film2','Nowak',2000,2);
INSERT INTO Film(id,tytul,rezyser,rok_wydania,id_kat) VALUES (3,'Film3','Kowalska',2000,1);
INSERT INTO Film(id,tytul,rezyser,rok_wydania,id_kat) VALUES (4,'Film4','Pisarz',2000,3);
INSERT INTO Film(id,tytul,rezyser,rok_wydania,id_kat) VALUES (5,'Film5','Jakastam',2000,4);
INSERT INTO Film(id,tytul,rezyser,rok_wydania,id_kat) VALUES (6,'Film6','Skurczynska',2000,5);
INSERT INTO Film(id,tytul,rezyser,rok_wydania,id_kat) VALUES (7,'Film7','Kowalski',2000,5);
