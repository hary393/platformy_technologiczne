delete from spells;
delete from mages;

insert into mages (id, circle, level, name) values (1, 'THIRD', 20, 'Irenicus');
insert into mages (id, circle, level, name) values (2, 'SECOND', 4, 'Imeon');
insert into mages (id, circle, level, name) values (3, 'FIRST', 2, 'Xart');

insert into spells (id, name, mage) values (1, 'Fire ball', 1);
insert into spells (id, name, mage) values (2, 'Fire wall', 1);
insert into spells (id, name, mage) values (3, 'Lightning bolt', 2);
insert into spells (id, name, mage) values (4, 'Shock', 2);
insert into spells (id, name, mage) values (5, 'Poison nova', 3);
