package pl.gda.pg.eti.kask.pt.mages.exceptions;

/**
 *
 * @author psysiu
 */
public class SpellException extends RuntimeException {

    public SpellException() {
    }

    public SpellException(String message) {
        super(message);
    }

    public SpellException(String message, Throwable cause) {
        super(message, cause);
    }

    public SpellException(Throwable cause) {
        super(cause);
    }

    public SpellException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
