package pl.gda.pg.eti.kask.pt.mages.model;

/**
 *
 * @author psysiu
 */
public enum Circle {

    INITIAL(0),
    FIRST(1),
    SECOND(3),
    THIRD(10);

    private int minLevel;

    private Circle(int minLevel) {
        this.minLevel = minLevel;
    }

    public int getMinLevel() {
        return minLevel;
    }
}
