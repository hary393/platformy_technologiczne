package pl.gda.pg.eti.kask.pt.mages.exceptions;

/**
 *
 * @author psysiu
 */
public class LevelException extends Exception {

    public LevelException() {
    }

    public LevelException(String message) {
        super(message);
    }

    public LevelException(String message, Throwable cause) {
        super(message, cause);
    }

    public LevelException(Throwable cause) {
        super(cause);
    }

    public LevelException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
