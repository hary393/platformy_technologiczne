package pl.gda.pg.eti.kask.pt.mages.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author psysiu
 */
@Entity
@Table(name = "spells")
public class Spell implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    
    @Column
    private String name;
    
    @ManyToOne
    @JoinColumn(name = "mage", referencedColumnName = "id")
    private Mage mage;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Spell(String name) {
        this.name = name;
    }

    public Spell() {
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Spell other = (Spell) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }
}
