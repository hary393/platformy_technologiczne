/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication5;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author PC10
 */
@Entity
@Table(name = "KATEDRA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Katedra.findAll", query = "SELECT k FROM Katedra k"),
    @NamedQuery(name = "Katedra.findByIdkatedry", query = "SELECT k FROM Katedra k WHERE k.idkatedry = :idkatedry"),
    @NamedQuery(name = "Katedra.findBySkrot", query = "SELECT k FROM Katedra k WHERE k.skrot = :skrot"),
    @NamedQuery(name = "Katedra.findByNazwa", query = "SELECT k FROM Katedra k WHERE k.nazwa = :nazwa")})
public class Katedra implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IDKATEDRY")
    private Integer idkatedry;
    @Column(name = "SKROT")
    private String skrot;
    @Column(name = "NAZWA")
    private String nazwa;
    @OneToMany(mappedBy = "idkatedry")
    private Collection<Student> studentCollection;

    public Katedra() {
    }

    public Katedra(Integer idkatedry) {
        this.idkatedry = idkatedry;
    }

    public Integer getIdkatedry() {
        return idkatedry;
    }

    public void setIdkatedry(Integer idkatedry) {
        this.idkatedry = idkatedry;
    }

    public String getSkrot() {
        return skrot;
    }

    public void setSkrot(String skrot) {
        this.skrot = skrot;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    @XmlTransient
    public Collection<Student> getStudentCollection() {
        return studentCollection;
    }

    public void setStudentCollection(Collection<Student> studentCollection) {
        this.studentCollection = studentCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idkatedry != null ? idkatedry.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Katedra)) {
            return false;
        }
        Katedra other = (Katedra) object;
        if ((this.idkatedry == null && other.idkatedry != null) || (this.idkatedry != null && !this.idkatedry.equals(other.idkatedry))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javafxapplication5.Katedra[ idkatedry=" + idkatedry + " ]";
    }
    
}
