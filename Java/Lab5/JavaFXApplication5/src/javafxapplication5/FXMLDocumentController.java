/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication5;

import static java.lang.Math.log;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.ChoiceBoxTableCell;
import javafx.scene.control.cell.TextFieldTableCell;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author PC10
 */
public class FXMLDocumentController implements Initializable {
    @FXML
    TextField Imie;
    @FXML
    TextField Nazwisko;
    private EntityManager em;
    @FXML
    private Label label;
    
    @FXML
    private TableView <Student> studentTableView;
    @FXML
    private TableView<Katedra> katedraTableView;
    @FXML
    private TableColumn studentNameColumn;
    @FXML
    private TableColumn studentNazwiskoColumn;
    @FXML
    private TableColumn studentWydzialColumn;
    @FXML
    private TableColumn katedraSkrotColumn;
    @FXML
    private TableColumn katedraNazwaColumn;
    

    private ObservableList<Student> studenci = FXCollections.observableArrayList();

    ListProperty<Katedra> katedry = new SimpleListProperty<>();
    private static final Logger log = Logger.getLogger(FXMLDocumentController.class.getName());
    
    
    @FXML
    private void dodajButton(ActionEvent event) {
        Student s = new Student();
        s.setImie(Imie.textProperty().getValue().toString());
        s.setNazwisko(Nazwisko.textProperty().getValue().toString());
        persist(s);
        studenci.add(s);
    }

    @FXML
    private void usunButton(ActionEvent event) {
        Student s = studentTableView.getSelectionModel().getSelectedItem();
        if (s != null) {
            studenci.remove(s);
            remove(s);
            studentTableView.getSelectionModel().clearSelection();
        }
    }    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        em = Persistence.createEntityManagerFactory("JavaFXApplication5PU").createEntityManager();
        studentTableView.setItems(studenci);
        katedraTableView.itemsProperty().bind(katedry);
        
        List<Student> dbStudents = em.createNamedQuery("Student.findAll").getResultList();
        studenci.addAll(dbStudents);
        studentTableView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Student>() {
            @Override
            public void changed(ObservableValue<? extends Student> value, Student oldValue, Student newValue) {
                if (newValue != null) {
                    katedry.set((ObservableList<Katedra>) newValue.getIdkatedry());
                } else {
                    katedry.setValue(null);
                }
            }
        });
        studentTableView.setEditable(true);
        
        
        studentNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        studentNameColumn.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<Student, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<Student, String> t) {
                t.getRowValue().setImie(t.getNewValue());
                update(t.getRowValue());
            }
        });
        studentNazwiskoColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        studentNazwiskoColumn.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<Student, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<Student, String> t) {
                t.getRowValue().setNazwisko(t.getNewValue());
                update(t.getRowValue());
            }
        });
        
       /* studentWydzialColumn.setCellFactory(ChoiceBoxTableCell.forTableColumn());
        studentWydzialColumn.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<Student, >>() {
            @Override
            public void handle(TableColumn.CellEditEvent<Student, > t) {
                t.getRowValue().setIdkatedry(t.getNewValue());
                update(t.getRowValue());
            }
        });*/
       //katedraNazwaColumn.setCellFactory(TextFieldTableCell.forTableColumn());
       //katedraSkrotColumn.setCellFactory(TextFieldTableCell.forTableColumn());
       
    }    
    private void update(Student mage) {
        try {
            em.getTransaction().begin();
            em.merge(mage);
            em.getTransaction().commit();
        } catch (Exception ex) { 
            log.log(Level.WARNING, ex.getMessage(), ex);
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
        }
    }
    private void remove(Student mage) {
        try {
            em.getTransaction().begin();
            em.remove(em.merge(mage));
            em.getTransaction().commit();
        } catch (Exception ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
        } finally {
        }
    }

    private void persist(Student mage) {
        try {
            em.getTransaction().begin();
            em.persist(mage);
            em.getTransaction().commit();
        } catch (Exception ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
        } finally {
        }
    }
}