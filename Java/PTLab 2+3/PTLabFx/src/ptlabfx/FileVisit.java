package ptlabfx;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import static java.nio.file.FileVisitResult.CONTINUE;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;

/**
 *
 * @author PC10
 */
public class FileVisit extends SimpleFileVisitor<Path> {
    private TreeView<File> treeView;
    private TreeItem<File> currentItem;
    
    public FileVisit(TreeView<File> tree) {
        treeView = tree;
        currentItem = null;
    }

    // Print information about
    // each type of file.
    @Override
    public FileVisitResult visitFile(Path file,
                                   BasicFileAttributes attr) {
        currentItem.getChildren().add(new TreeItem<File>(file.toFile()));
        return CONTINUE;
    }
    
    @Override
    public FileVisitResult preVisitDirectory(Path dir,
                                             BasicFileAttributes attrs) {
        TreeItem<File> ti = new TreeItem<File>(dir.toFile());
        if (currentItem == null) {
            treeView.setRoot(ti);
        } else {
            currentItem.getChildren().add(ti);
        }
        currentItem = ti;
        return FileVisitResult.CONTINUE;
    }
    
    @Override
    public FileVisitResult postVisitDirectory(Path dir,
                                          IOException exc) {
        currentItem = currentItem.getParent();
        return CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file,
                                       IOException exc) {
        System.err.println(exc);
        return CONTINUE;
    }
}