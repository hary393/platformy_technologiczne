/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ptlabfx;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Iterator;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.scene.control.TreeItem;

/**
 *
 * @author PC10
 */
public class FileClient extends Task<Void> {
    
    //String path=null;
    File plik=null;
    
    public FileClient(File meme){
    //this.path=droga;
    this.plik=meme;
    }

    
    @Override
    protected Void call() throws IOException, InterruptedException {  
        //Send file
        System.out.print("Przesyłanie "+plik.getName());
        Socket sock = new Socket("127.0.0.1", 6666);  //"localhost"
        DataOutputStream dos = new DataOutputStream(sock.getOutputStream());
        FileInputStream fis = new FileInputStream(plik);
        byte[] buffer = new byte[1024];
            dos.writeUTF(plik.getName());   
            int data=0;
            int post=0;
            while ((data=fis.read(buffer)) > 0) {
                dos.write(buffer);
                post += data;
                updateMessage("Przesłano" + post);
                updateProgress(post, plik.length());
                Thread.sleep(200);
            }
            fis.close();
            dos.close();
            sock.close(); 
    return null;
    }  
}