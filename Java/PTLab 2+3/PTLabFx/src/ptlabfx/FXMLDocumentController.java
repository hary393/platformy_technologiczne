/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ptlabfx;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.DosFileAttributes;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.stage.DirectoryChooser;
import javafx.util.Callback;

/**
 *
 * @author PC10
 */
public class FXMLDocumentController implements Initializable {
    @FXML
    private TreeView<File> treeView;
    @FXML
    private TextField Text;
    @FXML
    private ProgressBar progressBar;
    @FXML
    private Label label;

    DirectoryChooser chose = new DirectoryChooser();
    File file = chose.showDialog(null);
  
    TreeItem<File> root = new TreeItem<>();

    
    @FXML protected void RootchangeButton(ActionEvent event) throws IOException {
        File file = chose.showDialog(null);
        FileVisit ftb = new FileVisit(treeView);
        try {
            Files.walkFileTree(file.toPath(), ftb);
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
     @FXML protected void NewfileButton(ActionEvent event) throws IOException{
        String filename = Text.textProperty().get();
            if(!filename.isEmpty()) {
                TreeItem<File> selectedItem = treeView.getSelectionModel().getSelectedItem();
                File selectedFile = selectedItem.getValue();
                File newFile = new File(selectedFile, filename);
                newFile.createNewFile();
                String patch=selectedFile+ "\\" + filename;
                File hue=new File(patch);
                TreeItem<File> dodaj= new TreeItem<File>(hue);
                ObservableList<TreeItem<File>> selectedItem2= selectedItem.getChildren();
                selectedItem2.add(dodaj);
            }
     }
     
     @FXML protected void NewdirButton(ActionEvent event){
        String filename = Text.textProperty().get();
            if(!filename.isEmpty()) {
                TreeItem<File> selectedItem = treeView.getSelectionModel().getSelectedItem();
                File selectedFile = selectedItem.getValue();
                File newFile = new File(selectedFile, filename);
                newFile.mkdir();
                String patch=selectedFile+ "\\" + filename;
                File hue=new File(patch);
                TreeItem<File> dodaj= new TreeItem<File>(hue);
                ObservableList<TreeItem<File>> selectedItem2= selectedItem.getChildren();
                selectedItem2.add(dodaj);
            }
         
     }
     
     
     
    @FXML protected void DeleteButton(ActionEvent event) throws IOException {
        TreeItem<File> selected = treeView.getSelectionModel().getSelectedItem();
        //System.out.print(selected.getValue());
        Path path=Paths.get(selected.getValue().toString());
        try {
            FileDelete Deleter = new FileDelete();
            Files.walkFileTree(path, Deleter);
        } catch(IOException e) {
            System.err.println("Could not delete file/directory: " + e);
        }

        if (selected.getParent()!=null)
        selected.getParent().getChildren().removeAll(selected);
    }
    
    
    @FXML protected void SendButton(ActionEvent event){
            ObservableList<TreeItem<File>> selectedItems = treeView.getSelectionModel().getSelectedItems();
            //System.out.println(selectedItems.toString());
            //if(selected.getValue().isFile()){
            for (TreeItem<File> x : selectedItems) {
                File file = new File(x.getValue().toString()); 
                FileClient client =new FileClient(file);
                label.textProperty().bind(client.messageProperty());
                progressBar.progressProperty().bind(client.progressProperty());
                Thread thread = new Thread(client);
                thread.setDaemon(true);
                thread.start();
            }
            //}
     }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        treeView.setCellFactory(new FileCellFactory());
        FileVisit ftb = new FileVisit(treeView);
        treeView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        try {
            Files.walkFileTree(file.toPath(), ftb);
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    

    private static class FileCellFactory implements Callback<TreeView<File>, TreeCell<File>> {

        @Override
        public TreeCell<File> call(TreeView<File> p) {
        return new FileCell();
}
    }

    private static class FileCell extends TreeCell<File> {
        @Override
        protected void updateItem(File file, boolean empty) {
            super.updateItem(file, empty);
            if (file != null) {
                String rahs = "   ";
                try {
                    DosFileAttributes attr;
                    try {
                        attr = Files.readAttributes(file.toPath(), DosFileAttributes.class);
                        rahs += (attr.isReadOnly() ? "R" : " ");
                        rahs += (attr.isHidden() ? "H" : " ");
                        rahs += (attr.isArchive() ? "A" : " ");
                        rahs += (attr.isSystem() ? "S" : " ");
                        setText(file.getName()+rahs);
                    } catch (IOException ex) {
                        Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } catch (UnsupportedOperationException e) {
                    System.err.println("DOS file" + " attributes not supported:" + e);
                } 
            }
            else {
                setText(null);
            }
        }
    }
    
}
