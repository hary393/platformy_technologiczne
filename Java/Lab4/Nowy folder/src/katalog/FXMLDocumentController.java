package katalog;

import java.io.File;
import java.math.BigDecimal;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.StringConverter;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import product.*;


public class FXMLDocumentController implements Initializable
{
    @FXML
    private TableView<ProductType> itemsTableView;
    @FXML
    private TableColumn name;
    @FXML
    private TableColumn price;
    @FXML
    private TableColumn currency;
    @FXML
    private TextField nameTextField;
    @FXML
    private TextField priceTextField;
    @FXML
    private TextField currencyTextField;
    
    private static final Logger log = Logger.getLogger(FXMLDocumentController.class.getName());
    private ObservableList<ProductType> items = FXCollections.observableArrayList();
    Schema schema;
    
    
    @FXML
    private void handleButtonSave(ActionEvent event)
    {
        saveXML();
    }
    
    @FXML
    private void handleButtonLoad(ActionEvent event)
    {
        loadXML();
    }
    
    @FXML
    private void handleButtonAdd(ActionEvent event)
    {
        ProductType newProduct = new ProductType();
        newProduct.setName(nameTextField.getText());
        if (newProduct.getName().length() == 0)
            return;
        
        PriceType newPrice = new PriceType();
        newProduct.setPrice(newPrice);
        try
        {
            newPrice.setCurrency(CurrencyType.valueOf(currencyTextField.getText()));
            newPrice.setValue(BigDecimal.valueOf(Long.valueOf(priceTextField.getText())));
            
            items.add(newProduct);
        }
        catch (Exception ex)
        {}
    }
    
    @FXML
    private void handleButtonRemove(ActionEvent event)
    {
        ProductType productType = itemsTableView.getSelectionModel().getSelectedItem();
        if (productType != null)
            items.remove(productType);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        try
        {
            schema = sf.newSchema(new File("productsSchema.xsd"));
        }
        catch (SAXException ex)
        {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        itemsTableView.setEditable(true);
        itemsTableView.setItems(items);
        
        name.setCellValueFactory(new PropertyValueFactory("name"));
        name.setCellFactory(TextFieldTableCell.forTableColumn());
        name.setOnEditCommit(new EventHandler<CellEditEvent<ProductType, String>>()
        {
            @Override
            public void handle(CellEditEvent<ProductType, String> t)
            {
                ProductType productType = t.getRowValue();
                String newName = t.getNewValue();
                productType.setName(newName);
            }
        });
        
        price.setCellValueFactory(new PropertyValueFactory("price"));
        price.setCellFactory(TextFieldTableCell.forTableColumn(new StringConverter<PriceType>()
        {
            @Override
            public String toString(PriceType price)
            {
                return price.getValue().toString();
            }

            @Override
            public PriceType fromString(String string)
            {
                PriceType price = itemsTableView.getSelectionModel().getSelectedItem().getPrice();
                try
                {
                    price.setValue(BigDecimal.valueOf(Long.valueOf(string)));
                }
                catch (Exception ex)
                {}
                return price;
            }
        }));
        
        currency.setCellValueFactory(new PropertyValueFactory("price"));
        currency.setCellFactory(TextFieldTableCell.forTableColumn(new StringConverter<PriceType>()
        {
            @Override
            public String toString(PriceType price)
            {
                return price.getCurrency().toString();
            }

            @Override
            public PriceType fromString(String string)
            {
                PriceType price = itemsTableView.getSelectionModel().getSelectedItem().getPrice();
                try
                {
                    price.setCurrency(CurrencyType.valueOf(string));
                }
                catch (Exception ex)
                {}
                return price;
            }
        }));
        
        loadXML();
    }    

    private class ProductListCell extends ListCell<ProductType>
    {
        @Override
        protected void updateItem(ProductType productType, boolean empty)
        {
            super.updateItem(productType, empty);
            if (productType != null)
            {
                //textProperty().bind(productType.));
            } else if (textProperty().isBound())
            {
                textProperty().unbind();
                setText(null);
            }
        }
    }
    
    private void saveXML()
    {
        try
        {
            JAXBContext context = JAXBContext.newInstance(ObjectFactory.class.getPackage().getName());
            Marshaller marshaller = context.createMarshaller();
            if (schema != null)
                marshaller.setSchema(schema);
            
            ProductCatalog productCatalog = new ProductCatalog();
            productCatalog.setProduct(new ArrayList<>(items));
            
            marshaller.marshal(productCatalog, new DefaultHandler());
            marshaller.marshal(productCatalog, new File("products.xml"));
        }
        catch (Exception ex)
        {
            //log.log(Level.WARNING, ex.getMessage(), ex);
        }
    }
    
    private void loadXML()
    {
        try
        {
            JAXBContext context = JAXBContext.newInstance(ObjectFactory.class.getPackage().getName());
            Unmarshaller unmarshaller = context.createUnmarshaller();
            if (schema != null)
                unmarshaller.setSchema(schema);
            
            ProductCatalog productCatalog = (ProductCatalog) unmarshaller.unmarshal(new File("products.xml"));
            
            items.clear();
            items.addAll(productCatalog.getProduct());
        }
        catch (JAXBException ex)
        {
            //log.log(Level.WARNING, ex.getMessage(), ex);
        }
    }
    
}