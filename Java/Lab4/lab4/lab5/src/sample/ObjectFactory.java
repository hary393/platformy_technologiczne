package sample;

import javax.xml.bind.annotation.XmlRegistry;

@XmlRegistry
public class ObjectFactory {

    public ObjectFactory() {
    }

    public Tower createTower() {
        return new Tower();
    }

    public Book createBook() {
        return new Book();
    }


}
