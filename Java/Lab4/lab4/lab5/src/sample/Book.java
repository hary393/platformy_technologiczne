package sample;



import javafx.beans.property.SimpleStringProperty;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlType(name = "book", propOrder = {"title", "pages"})
public class Book  {
    private final SimpleStringProperty title ;
    private final SimpleStringProperty pages ;



    Book(){
        title =new SimpleStringProperty("");
        pages = new SimpleStringProperty();
    }
    Book(String t, String p){
        this.title = new SimpleStringProperty(t);
        this.pages = new SimpleStringProperty(p);
    }

    final public String getTitle() {
        return title.get();
    }
    final public String getPages() {
        return pages.get();
    }
    @XmlAttribute
    public void setPages(String p){
        pages.set(p);
    }
    @XmlAttribute
    public void setTitle(String p){
        title.set(p);
    }
}
