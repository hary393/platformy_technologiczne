package sample;
import java.util.HashSet;
import java.util.Set;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author psysiu
 */
@XmlRootElement
public class Tower {

    @XmlElement(required = true, name = "book")
    private Set<Book> books = new HashSet<>();

    public Tower() {
    }

    public Tower(Set<Book> b) {
        this.books = b;
    }

    public Set<Book> getMages() {
        return books;
    }

    public void setBooks(Set<Book> b) {
        this.books = b;
    }
}
