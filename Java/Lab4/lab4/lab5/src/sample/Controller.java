package sample;

import java.io.File;
import java.net.URL;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.shape.Path;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.util.converter.NumberStringConverter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.soap.Text;

public class Controller extends Control implements Initializable  {
    final ObservableList<Book> books = FXCollections.observableArrayList();
    @FXML
    TextField pLab;
    @FXML
    TextField tLab;
    @FXML
    Label path;
    @FXML
    TextField name;
    @FXML
    TableView <Book> tableView ;
    @FXML
    TableColumn<Book, String> titleColumn;
    @FXML
    TableColumn<Book, String> pageColumn;


    @FXML
    private void addAction(ActionEvent event){
        String ti = tLab.textProperty().getValue().toString();
        String pa = pLab.textProperty().getValue().toString();
        Book b = new Book(ti, pa);
        books.add(b);
    }

    @FXML
    private void deleteAction(ActionEvent event){
        Book b;
        b = tableView.getSelectionModel().getSelectedItem();
        books.remove(b);

    }
    @FXML
    private void readAction(ActionEvent event){
        FileChooser chooser = new FileChooser();
        File file = chooser.showOpenDialog(null);
        if (file != null) {
            try {
                JAXBContext context = JAXBContext.newInstance(ObjectFactory.class.getPackage().getName());
                Unmarshaller unmarshaller = context.createUnmarshaller();
                Tower tower = (Tower) unmarshaller.unmarshal(file);
                books.clear();
                System.out.println(books.size());
                books.addAll(tower.getMages());
            } catch (JAXBException ex) {
                // log.log(Level.WARNING, ex.getMessage(), ex);
                System.err.println(ex);
            }
        }
    }
    @FXML
      private void writeAction(ActionEvent event) {

        Tower tower = new Tower();
        try {
            String tmp = path.getText()+"\\"+name.getText();
            System.out.println(tmp);
            File file = new File(tmp);
            JAXBContext jaxbContext = JAXBContext.newInstance(ObjectFactory.class.getPackage().getName());
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            Tower t = new Tower();
            t.setBooks(new HashSet<>(books));

            jaxbMarshaller.marshal(t, file);
            jaxbMarshaller.marshal(t, System.out);

        } catch (JAXBException e) {
            e.printStackTrace();
        }


    }
    @FXML
    private void browseAction(ActionEvent event) {
        DirectoryChooser chooser = new DirectoryChooser();
        //File file = chooser.showOpenDialog(null);
        File p = chooser.showDialog(null);
        path.setText(p.toPath().toString());


    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {


        titleColumn.setCellValueFactory(new PropertyValueFactory<Book, String>("title"));
        pageColumn.setCellValueFactory(new PropertyValueFactory<Book, String>("pages"));

        tableView.setItems(books);
        tableView.setEditable(true);

        titleColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        titleColumn.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<Book, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<Book, String> event) {
                Book b = event.getRowValue();
                String newT = event.getNewValue();
                b.setTitle(newT);
            }
        });

        pageColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        pageColumn.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<Book, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<Book, String> event) {
                Book b = event.getRowValue();
                String newP=event.getNewValue();
                b.setPages(newP);
            }
        });

    }

}
