/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaxml;

import generated.ObjectFactory;
import generated.ProductCatalog;
import generated.ProductCatalog.Product;
import generated.ProductCatalog.Product.Price;
import java.io.File;
import java.math.BigDecimal;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.FileChooser;
import javafx.util.StringConverter;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author PC10
 */
public class FXMLDocumentController implements Initializable {
    ProductCatalog Catalog= new ProductCatalog();
    @FXML
    TextField name;
    @FXML
    TextField price;
    @FXML
    TextField currency;
    @FXML
    TableView <ProductCatalog.Product> tableView ;
    @FXML
    TableColumn<ProductCatalog.Product, String> nameColumn;
    @FXML
    TableColumn<ProductCatalog.Product, String> priceColumn;
    @FXML
    TableColumn<ProductCatalog.Product, String> currencyColumn;

    public FXMLDocumentController() {
    }
    
    @FXML
    private void readXmlButton(ActionEvent event){
        FileChooser chooser = new FileChooser();
        File file = chooser.showOpenDialog(null);
        if (file != null) {
            try {
                JAXBContext context = JAXBContext.newInstance(ObjectFactory.class.getPackage().getName());
                Unmarshaller unmarshaller = context.createUnmarshaller();
                unmarshaller.setEventHandler(new MyValidationEventHandler());
                ProductCatalog CatalogN = (ProductCatalog) unmarshaller.unmarshal(file);
                Catalog.getProduct().clear();
                System.out.println("1");
                Catalog.getProduct().addAll(CatalogN.getProduct());
            } catch (JAXBException ex) {
                // log.log(Level.WARNING, ex.getMessage(), ex);
                System.err.println(ex);
            }
        }
    }
     @FXML
    private void saveXmlButton(ActionEvent event) throws SAXException {
        FileChooser chooser = new FileChooser();
        File file = chooser.showSaveDialog(null);
        if (file != null) {
            try {
                 SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI); 
                Schema schema = sf.newSchema(new File("schema.xsd")); 
                JAXBContext context = JAXBContext.newInstance(ObjectFactory.class.getPackage().getName());
                Marshaller marshaller = context.createMarshaller();
                marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                marshaller.setSchema(schema);
                marshaller.setEventHandler(new MyValidationEventHandler());
                ProductCatalog CatalogN=new ProductCatalog();
                CatalogN.getProduct().addAll(Catalog.getProduct());
                marshaller.marshal(CatalogN, file);
            } catch (JAXBException ex) {
                // log.log(Level.WARNING, ex.getMessage(), ex);
                System.err.println(ex);
            }
        }
    }
    @FXML
    private void dodajButton(ActionEvent event){
        String Nazw = name.textProperty().getValue().toString();
        String Cenowo=price.textProperty().getValue().toString();
        BigDecimal Cen=  BigDecimal.valueOf(Double.valueOf(Cenowo));
        String Curr = currency.textProperty().getValue().toString();
        Product nowy=new Product(Nazw, Cen ,Curr);
        Catalog.getProduct().add(nowy);
    }
    
    @FXML
    private void usuńButton(ActionEvent event){
        Product dousun=tableView.getSelectionModel().getSelectedItem();
        Catalog.getProduct().remove(dousun);
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       // nameColumn.setCellValueFactory(new PropertyValueFactory<ProductCatalog.Product, String>("Name"));
      //  priceColumn.setCellValueFactory(new PropertyValueFactory<ProductCatalog.Product, String>("Price"));
       // currencyColumn.setCellValueFactory(new PropertyValueFactory<ProductCatalog.Product, String>("Currency"));

        tableView.setItems((ObservableList<ProductCatalog.Product>) Catalog.getProduct());
        tableView.setEditable(true);
        priceColumn.setEditable(true);
        currencyColumn.setEditable(true);

        nameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        nameColumn.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<ProductCatalog.Product, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<ProductCatalog.Product, String> event) {
                Product b = event.getRowValue();
                String newT = event.getNewValue();
                b.setName(newT);
            }
        });
            
        
        
        priceColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        priceColumn.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<ProductCatalog.Product, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<ProductCatalog.Product, String> event) {
                try {
                    
                 
                Product b = event.getRowValue();
                String newP=event.getNewValue();
                Price newPrice = new Price();
                newPrice.setCurrency(tableView.getSelectionModel().getSelectedItem().getPrice().getCurrency());
                newPrice.setValue(BigDecimal.valueOf(Double.valueOf(newP)));
                b.setPrice(newPrice);
                }
                catch (NumberFormatException e) {
                  
                }
            }
        });
        currencyColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        currencyColumn.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<ProductCatalog.Product, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<ProductCatalog.Product, String> event) {
                Product b = event.getRowValue();
                String newP=event.getNewValue();
                String dec=tableView.getSelectionModel().getSelectedItem().getPrice().getCurrency();
                Price newPrice = new Price();
                newPrice.setValue(BigDecimal.valueOf(Double.valueOf(dec)));
                newPrice.setCurrency(event.getNewValue());
                b.setPrice(newPrice);
            }
        });
    }    
    public class MyValidationEventHandler implements ValidationEventHandler {
 
    public boolean handleEvent(ValidationEvent event) {
        System.out.println("\nEVENT");
        System.out.println("SEVERITY:  " + event.getSeverity());
        System.out.println("MESSAGE:  " + event.getMessage());
        System.out.println("LINKED EXCEPTION:  " + event.getLinkedException());
        System.out.println("LOCATOR");
        System.out.println("    LINE NUMBER:  " + event.getLocator().getLineNumber());
        System.out.println("    COLUMN NUMBER:  " + event.getLocator().getColumnNumber());
        System.out.println("    OFFSET:  " + event.getLocator().getOffset());
        System.out.println("    OBJECT:  " + event.getLocator().getObject());
        System.out.println("    NODE:  " + event.getLocator().getNode());
        System.out.println("    URL:  " + event.getLocator().getURL());
        return false;
    }
 
}
}

