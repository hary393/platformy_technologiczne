﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Xml.XPath;

namespace LabPT3
{

    public class Engine
    {
        public double displacment;
        public double horsepower;
        [XmlAttribute]
        public string model;


        public Engine(double dis,double horse,string mod) 
        {
            displacment = dis;
            horsepower = horse;
            model = mod;

        }
        public Engine() { }
    }


    [XmlType("car")]
    [XmlRoot("car")]
    public class Car
    {
        public string model;
        [XmlElement("engine")]
        public Engine motor;
        public int year;

        
        public Car(string mod,Engine en,int yer)
        {
            model = mod;

            motor = en;
            year = yer;

        }
        public Car() { }
    }



    class Program
    {

        static void Main(string[] args)
        {

            List<Car> myCars = new List<Car>(){
                new Car("E250", new Engine(1.8, 204, "CGI"), 2009),
                new Car("E350", new Engine(3.5, 292, "CGI"), 2009),
                new Car("A6", new Engine(2.5, 187, "FSI"), 2012),
                new Car("A6", new Engine(2.8, 220, "FSI"), 2012),
                new Car("A6", new Engine(3.0, 295, "TFSI"), 2012),
                new Car("A6", new Engine(2.0, 175, "TDI"), 2011),
                new Car("A6", new Engine(3.0, 309, "TDI"), 2011),
                new Car("S6", new Engine(4.0, 414, "TFSI"), 2012),
                new Car("S8", new Engine(4.0, 513, "TFSI"), 2012)
                };

            createXmlFromLinq(myCars);

            var carQuery1 =
                from car in myCars
                where car.model.Equals("A6")
                let x = car.motor.horsepower/car.motor.displacment
                let y = (car.motor.model.Equals("TDI") ? "diesel":"petrol")
                select new{eingineType=y, hppl=x};



            var carQuery2 = from r in carQuery1
                            group r by new
                            {
                                Type = r.eingineType,
                            } into g
                            select new
                            {
                                Type = g.Key.Type,
                                count=g.Count(),
                                hppl = g.Sum(x => x.hppl)
                            };


            foreach (var item in carQuery2)
            {
                Console.WriteLine("Eingine type: {0}, Hppl: {1}", item.Type, item.hppl/item.count);
            }



            XmlAttributeOverrides overrides = new XmlAttributeOverrides();
            XmlAttributes attr = new XmlAttributes();
            attr.XmlRoot = new XmlRootAttribute("cars");
            overrides.Add(typeof(List<Car>), attr);
            using (var writer = new System.IO.StreamWriter("kek.xml"))
            {
                var serializer = new XmlSerializer(typeof(List<Car>),overrides);
                serializer.Serialize(writer, myCars);
                writer.Flush();
            }

            using (var stream = System.IO.File.OpenRead("kek.xml"))
            {

                var serializer = new XmlSerializer(typeof(List<Car>),overrides);
               List<Car> myCars2 = serializer.Deserialize(stream) as List<Car>;
               foreach (var item in myCars2)
               {
                   Console.WriteLine("Eingine type: {0}, Hppl: {1} {2} {3}", item.year, item.model, item.motor.displacment, item.motor.horsepower);
               }
            }


            string xpath1 = "(sum(//engine[@model!='TDI']/horsepower) div count(//engine[@model!='TDI']/horsepower))";
            XElement rootNode = XElement.Load("kek.xml");
            double avgHP = (double)rootNode.XPathEvaluate(xpath1);
            string xpath2 = "(//model[not(preceding:: model/.=.)])"; 
            IEnumerable<XElement> models = rootNode.XPathSelectElements(xpath2);


            XmlDocument document = new XmlDocument();
            document.Load("kek.xml");
            XPathNavigator navigator = document.CreateNavigator();

            foreach (var item in rootNode.XPathSelectElements("//car"))
            {
                XElement rok = rootNode.XPathSelectElement("//year");
                item.XPathSelectElement("model").Add(new XAttribute("year",rok.Value));
                rootNode.XPathSelectElement("//horsepower").Name = "hp";
                rok.Remove();
            }



            IEnumerable<XElement> models2 =rootNode.XPathSelectElements("(//car)");
            Console.WriteLine(rootNode.ToString());

            XElement tmp = XElement.Load("Cars2.html");
            XElement body = (XElement)tmp.LastNode;

            IEnumerable<XElement> nodes =
                from car in myCars
                select new XElement("tr",
                    new XElement("td", car.model),
                    new XElement("td",car.motor.model ),
                    new XElement("td", car.motor.displacment),
                    new XElement("td", car.motor.horsepower),
                    new XElement("td", car.year)
                    );
            XElement tble = new XElement("table");
            tble.Add(nodes);
            body.Add(tble);
            tmp.Save("Cars3.html");
        }


        private static void createXmlFromLinq(List<Car> myCars)
        {
            IEnumerable<XElement> nodes = 
                from car in myCars
                    select new XElement("car",
                        new XElement("model",car.model),
                        new XElement("engine", new XAttribute("model", car.motor.model),
                            new XElement("displacment", car.motor.displacment),
                            new XElement("horsepower", car.motor.horsepower)
                            ),
                        new XElement("year", car.year)
                        );
            XElement rootNode = new XElement("cars", nodes); //create a root node to contain the query results
            rootNode.Save("CarsFromLinq.xml");
        }

    }
}
