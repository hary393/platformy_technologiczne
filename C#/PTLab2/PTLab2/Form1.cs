﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace PTLab2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        FolderBrowserDialog brw = new FolderBrowserDialog();

        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void BuildTree(DirectoryInfo directoryInfo, TreeNodeCollection addInMe)
        {
            TreeNode curNode = addInMe.Add(directoryInfo.Name);

            foreach (DirectoryInfo subdir in directoryInfo.GetDirectories())
            {
                BuildTree(subdir, curNode.Nodes);
            }
            foreach (FileInfo file in directoryInfo.GetFiles())
            {
                curNode.Nodes.Add(file.FullName, file.Name);
            }
        }
        private void DeleteDir(DirectoryInfo directoryInfo)
        {

            foreach (DirectoryInfo subdir in directoryInfo.GetDirectories())
            {
                DeleteDir(subdir);
                if ((subdir.Attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                    subdir.Attributes^= FileAttributes.ReadOnly;
                subdir.Delete();
            }
            foreach (FileInfo file in directoryInfo.GetFiles())
            {
                if ((file.Attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                    file.Attributes ^= FileAttributes.ReadOnly;
                file.Delete();
            }
        }

   

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            brw = new FolderBrowserDialog();
            if (brw.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                DirectoryInfo hue = new DirectoryInfo(brw.SelectedPath);
                BuildTree(hue, treeView1.Nodes);
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (treeView1.SelectedNode != null)
            {
                String nazwa = Directory.GetParent(brw.SelectedPath) + "\\" + treeView1.SelectedNode.FullPath;
                FileInfo file = new FileInfo(nazwa);
                treeView1.Nodes.Remove(treeView1.SelectedNode);
                MessageBox.Show(nazwa);
                if ((file.Attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly) { 
                    file.Attributes ^= FileAttributes.ReadOnly;}
                if ((file.Attributes & FileAttributes.Directory) == FileAttributes.Directory)
                {
                    DirectoryInfo dir = new DirectoryInfo(nazwa);
                    DeleteDir(dir);
                    dir.Delete();
                }
                else
                file.Delete();
            }
        }

        private void createToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (treeView1.SelectedNode != null)
            {
                Form2 f2 = new Form2();
                f2.ShowDialog();
                if (f2.exittype==1)
                {
                    String path;
                    FileAttributes attr = File.GetAttributes(Directory.GetParent(brw.SelectedPath)+"\\" + treeView1.SelectedNode.FullPath);
                    if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
                    {
                        path = Directory.GetParent(brw.SelectedPath) + "\\" + treeView1.SelectedNode.FullPath;
                    }
                    else
                        path = Directory.GetParent(brw.SelectedPath) + "\\" + treeView1.SelectedNode.Parent.FullPath;

                    if (f2.type == 1)
                    {
                        string pattern = @"^([a-zA-Z0-9]|_|~|-){1,8}.+(txt|php|htm)";
                        if (Regex.IsMatch(f2.nazwa, pattern))
                        {
                            var stream=System.IO.File.Create(System.IO.Path.Combine(path, f2.nazwa));
                            FileInfo file = new FileInfo(System.IO.Path.Combine(path, f2.nazwa));
                            FileInfo fileroot = new FileInfo(path);
                            file.Attributes = f2.atr;
                            if ((fileroot.Attributes & FileAttributes.Directory) == FileAttributes.Directory)
                                treeView1.SelectedNode.Nodes.Add(file.Name);
                            else
                                treeView1.SelectedNode.Parent.Nodes.Add(file.Name);
                            stream.Close();
                        }
                        else
                            MessageBox.Show("Błędna nazwa lub rozszerzenie");
                    }
                    else
                    {
                        FileInfo fileroot = new FileInfo(path);
                        System.IO.Directory.CreateDirectory(System.IO.Path.Combine(path, f2.nazwa));
                        DirectoryInfo dir = new DirectoryInfo(System.IO.Path.Combine(path, f2.nazwa));
                        dir.Attributes = f2.atr;
                        if ((fileroot.Attributes & FileAttributes.Directory) == FileAttributes.Directory)
                            treeView1.SelectedNode.Nodes.Add(dir.Name);
                        else
                            treeView1.SelectedNode.Parent.Nodes.Add(dir.Name);
                    }
                }

            }
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            switch ((e.Action))
            {
                default:
                    String nazwa = Directory.GetParent(brw.SelectedPath) + "\\" + treeView1.SelectedNode.FullPath;
                    FileInfo file = new FileInfo(nazwa);
                    if (file.Extension.Equals(".txt"))
                    {
                        String line="";
                        try
                            {   // Open the text file using a stream reader.
                                using (StreamReader sr = new StreamReader(nazwa))
                                {
	                            // Read the stream to a string
                                    line = sr.ReadToEnd();
                                    sr.Close();
                                }
                            }
                            catch (Exception q)
                            {
                                Console.WriteLine("The file could not be read:");
                                Console.WriteLine(q.Message);
                            }
                        textBox1.Text=line;


                    }
                    else
                        textBox1.Text = "";
                    string atr = "";
                    string lel = file.Attributes.ToString();
                    if (lel.Contains("ReadOnly"))
                        atr += "r";
                    else
                        atr += "-";
                    if (lel.Contains("Archive"))
                        atr += "a";
                    else
                        atr += "-";
                    if (lel.Contains("System"))
                        atr += "s";
                    else
                        atr += "-";
                    if (lel.Contains("Hidden"))
                        atr += "h";
                    else
                        atr += "-";
                    toolStripStatusLabel1.Text = atr;
                    break;
            }
        }

    
    }
}
