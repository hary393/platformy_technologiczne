﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PTLab2
{
    public partial class Form2 : Form
    {

        public String nazwa;
        public FileAttributes atr;
        public int type;
        public int exittype;

        public Form2()
        {
            InitializeComponent();
            radioButton1.Checked=true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            exittype = 0;
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!textBox1.ToString().Equals(""))
            {
                nazwa = textBox1.Text;
                bool isChecked = radioButton1.Checked;
                if (isChecked)
                {
                    type = 1;
                }
                else
                {
                    type = 2;
                }
                if (checkBox1.Checked)
                {
                    atr |= FileAttributes.ReadOnly;
                }
                if (checkBox2.Checked)
                {
                    atr |= FileAttributes.Archive;
                }
                if (checkBox3.Checked)
                {
                    atr |= FileAttributes.Hidden;
                }
                if (checkBox4.Checked)
                {
                    atr |= FileAttributes.System;
                }
                exittype = 1;
                this.Close();
            }
            else
            {
                MessageBox.Show("Wypełnij Forme!");
            }
        }


    }
}
