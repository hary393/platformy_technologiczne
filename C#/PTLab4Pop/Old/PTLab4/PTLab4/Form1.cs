﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PTLab4
{


    public partial class Form1 : Form
    {
        public Form1()
        {
            List<Car> myCars = new List<Car>(){
            new Car("E250", new Engine(1.8, 204, "CGI"), 2009),
            new Car("E350", new Engine(3.5, 292, "CGI"), 2009),
            new Car("A6", new Engine(2.5, 187, "FSI"), 2012),
            new Car("A6", new Engine(2.8, 220, "FSI"), 2012),
            new Car("A6", new Engine(3.0, 295, "TFSI"), 2012),
            new Car("A6", new Engine(2.0, 175, "TDI"), 2011),
            new Car("A6", new Engine(3.0, 309, "TDI"), 2011),
            new Car("S6", new Engine(4.0, 414, "TFSI"), 2012),
            new Car("S8", new Engine(4.0, 513, "TFSI"), 2012)
            };

            InitializeComponent();
            //Query syntax:
            var elements =
                from z in(
                from r in
                (from car in myCars
                where car.model.Equals("A6")
                let x = car.motor.horsepower / car.motor.displacment
                let y = (car.motor.model.Equals("TDI") ? "diesel" : "petrol")
                select new  { engineType = y, HPPL = x })
                group r by new
                {
                    Type = r.engineType,
                } into g
                select new
                {
                    engineType = g.Key.Type,
                    avgHPPL = g.Sum(x => x.HPPL / g.Count())
                })
                 orderby z.avgHPPL descending
                    select z;
                
   
            foreach (var e in elements) MessageBox.Show(e.engineType + ": " + e.avgHPPL);

            //Method syntax:
            var elements2 = myCars.Where(a => a.model.Equals("A6")).Select(car1 => new
            {
                engineType = (car1.motor.model.Equals("TDI") ? "diesel" : "petrol"),
                HPPL = car1.motor.horsepower / car1.motor.displacment
            }
            ).GroupBy(car => car.engineType).Select(carg => new
            {
                engineType = carg.Key,
                avgHPPL = carg.Average(x=> x.HPPL)
            }).OrderByDescending(car => car.avgHPPL);

            foreach (var e in elements2) MessageBox.Show(e.engineType + ": " + e.avgHPPL);





            Func<Car, Car, int> arg1 = delegate(Car car1, Car car2) { return (int)(-1*car1.motor.horsepower - car2.motor.horsepower); };
            myCars.Sort(new Comparison<Car>(arg1));
            Predicate<Car> arg2 = (Car p) => { return p.motor.model == "TDI"; };

            Action<Car> arg3 = delegate(Car a) { MessageBox.Show(a.model+" "+a.motor.displacment+" "+a.motor.horsepower+" "+a.motor.model+" "+a.year ); };

            myCars.FindAll(arg2).ForEach(arg3);

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            List<Car> myCars = new List<Car>(){
            new Car("E250", new Engine(1.8, 204, "CGI"), 2009),
            new Car("E350", new Engine(3.5, 292, "CGI"), 2009),
            new Car("A6", new Engine(2.5, 187, "FSI"), 2012),
            new Car("A6", new Engine(2.8, 220, "FSI"), 2012),
            new Car("A6", new Engine(3.0, 295, "TFSI"), 2012),
            new Car("A6", new Engine(2.0, 175, "TDI"), 2011),
            new Car("A6", new Engine(3.0, 309, "TDI"), 2011),
            new Car("S6", new Engine(4.0, 414, "TFSI"), 2012),
            new Car("S8", new Engine(4.0, 513, "TFSI"), 2012)
            };
            BindingList<Car> myCarsBindingList = new BindingList<Car>(myCars);
            BindingSource carBindingSource = new BindingSource();
            carBindingSource.DataSource = myCarsBindingList;
            //Drag a DataGridView control from the Toolbox to the form.
            dataGridView1.DataSource = carBindingSource;
        }

    }


    public class Engine
    {
        public double displacment {get;set;}
        public double horsepower { get; set; }
        public string model { get; set; }

        public Engine(double dis, double horse, string mod)
        {
            displacment = dis;
            horsepower = horse;
            model = mod;

        }
        public Engine() { }
        public String toString(){
            return displacment + " " + horsepower + " " + model;
        }
    }

    public class Car
    {
        public string model { get; set; }
        public Engine motor{get; set;}
        public int year { get; set; }



        public Car(string mod, Engine en, int yer)
        {
            model = mod;
            motor = en;
            year = yer;

        }
        public Car() { }
    }


}
