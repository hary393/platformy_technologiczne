﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab4
{
    public partial class AddForm : Form
    {
        public Car createdCar { get; set; }
        public Boolean exitViaOk { get; set; }

        public AddForm()
        {
            InitializeComponent();

        }

        void updateStatus()
        {
            createdCar = new Car();
            createdCar.Model = modelTextBox.Text;
            createdCar.Year = int.Parse(yearTextBox.Text);

            createdCar.Engine = new CarEngine();
            createdCar.Engine.Displacement = double.Parse(engineDisplacementTextBox.Text);
            createdCar.Engine.HorsePower = double.Parse(engineHorsePowerTextBox.Text);
            createdCar.Engine.Model = engineModelTextBox.Text;
        }

        void exit(Boolean status)
        {
            exitViaOk = status;
            this.Close();
        }

        private void AddClick(object sender, EventArgs e)
        {
            try
            {
                updateStatus();
                exit(true);
            }
            catch (Exception)
            {
                MessageBox.Show(this, "Some fields have invalid values.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void CancelClick(object sender, EventArgs e)
        {
            exit(false);
        }
    }
}
