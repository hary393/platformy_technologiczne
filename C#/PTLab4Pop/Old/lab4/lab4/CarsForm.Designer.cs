﻿namespace lab4
{
    partial class CarsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.searchInComboBox = new System.Windows.Forms.ComboBox();
            this.findButton = new System.Windows.Forms.Button();
            this.searchInLabel = new System.Windows.Forms.Label();
            this.searchForLabel = new System.Windows.Forms.Label();
            this.searchForTextBox = new System.Windows.Forms.TextBox();
            this.carGridView = new System.Windows.Forms.DataGridView();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.addButton = new System.Windows.Forms.Button();
            this.removeButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.carGridView)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(448, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // searchInComboBox
            // 
            this.searchInComboBox.FormattingEnabled = true;
            this.searchInComboBox.Location = new System.Drawing.Point(264, 2);
            this.searchInComboBox.Name = "searchInComboBox";
            this.searchInComboBox.Size = new System.Drawing.Size(121, 21);
            this.searchInComboBox.TabIndex = 1;
            this.searchInComboBox.Enter += new System.EventHandler(this.searchInComboBox_Enter);
            // 
            // findButton
            // 
            this.findButton.Location = new System.Drawing.Point(391, 2);
            this.findButton.Name = "findButton";
            this.findButton.Size = new System.Drawing.Size(41, 23);
            this.findButton.TabIndex = 2;
            this.findButton.Text = "Find";
            this.findButton.UseVisualStyleBackColor = true;
            this.findButton.Click += new System.EventHandler(this.findButton_Click);
            // 
            // searchInLabel
            // 
            this.searchInLabel.AutoSize = true;
            this.searchInLabel.Location = new System.Drawing.Point(206, 7);
            this.searchInLabel.Name = "searchInLabel";
            this.searchInLabel.Size = new System.Drawing.Size(52, 13);
            this.searchInLabel.TabIndex = 3;
            this.searchInLabel.Text = "Search in";
            // 
            // searchForLabel
            // 
            this.searchForLabel.AutoSize = true;
            this.searchForLabel.Location = new System.Drawing.Point(12, 7);
            this.searchForLabel.Name = "searchForLabel";
            this.searchForLabel.Size = new System.Drawing.Size(56, 13);
            this.searchForLabel.TabIndex = 4;
            this.searchForLabel.Text = "Search for";
            // 
            // searchForTextBox
            // 
            this.searchForTextBox.Location = new System.Drawing.Point(74, 2);
            this.searchForTextBox.Name = "searchForTextBox";
            this.searchForTextBox.Size = new System.Drawing.Size(100, 20);
            this.searchForTextBox.TabIndex = 5;
            // 
            // carGridView
            // 
            this.carGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.carGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.carGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.carGridView.Location = new System.Drawing.Point(0, 25);
            this.carGridView.Name = "carGridView";
            this.carGridView.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.carGridView.Size = new System.Drawing.Size(448, 358);
            this.carGridView.TabIndex = 6;
            this.carGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.carGridView_CellEndEdit);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.addButton);
            this.flowLayoutPanel1.Controls.Add(this.removeButton);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 353);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(448, 30);
            this.flowLayoutPanel1.TabIndex = 7;
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(3, 3);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 23);
            this.addButton.TabIndex = 0;
            this.addButton.Text = "Add";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // removeButton
            // 
            this.removeButton.Location = new System.Drawing.Point(84, 3);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(75, 23);
            this.removeButton.TabIndex = 1;
            this.removeButton.Text = "Remove";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
            // 
            // CarsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(448, 383);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.carGridView);
            this.Controls.Add(this.searchForTextBox);
            this.Controls.Add(this.searchForLabel);
            this.Controls.Add(this.searchInLabel);
            this.Controls.Add(this.findButton);
            this.Controls.Add(this.searchInComboBox);
            this.Controls.Add(this.toolStrip1);
            this.Name = "CarsForm";
            this.Text = "Cars";
            ((System.ComponentModel.ISupportInitialize)(this.carGridView)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ComboBox searchInComboBox;
        private System.Windows.Forms.Button findButton;
        private System.Windows.Forms.Label searchInLabel;
        private System.Windows.Forms.Label searchForLabel;
        private System.Windows.Forms.TextBox searchForTextBox;
        private System.Windows.Forms.DataGridView carGridView;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button removeButton;
    }
}

