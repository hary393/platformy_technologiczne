﻿namespace lab4
{
    partial class AddForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.titleLabel = new System.Windows.Forms.Label();
            this.modelLabel = new System.Windows.Forms.Label();
            this.modelTextBox = new System.Windows.Forms.TextBox();
            this.engineGroupBox = new System.Windows.Forms.GroupBox();
            this.engineDisplacementTextBox = new System.Windows.Forms.TextBox();
            this.engineDisplacementLabel = new System.Windows.Forms.Label();
            this.engineHorsePowerTextBox = new System.Windows.Forms.TextBox();
            this.engineHorserPowerLabel = new System.Windows.Forms.Label();
            this.engineModelTextBox = new System.Windows.Forms.TextBox();
            this.engineModelLabel = new System.Windows.Forms.Label();
            this.yearTextBox = new System.Windows.Forms.TextBox();
            this.yearLabel = new System.Windows.Forms.Label();
            this.addButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.engineGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.Font = new System.Drawing.Font("Calibri", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.titleLabel.Location = new System.Drawing.Point(81, 9);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(116, 26);
            this.titleLabel.TabIndex = 0;
            this.titleLabel.Text = "Car creation";
            // 
            // modelLabel
            // 
            this.modelLabel.AutoSize = true;
            this.modelLabel.Location = new System.Drawing.Point(13, 56);
            this.modelLabel.Name = "modelLabel";
            this.modelLabel.Size = new System.Drawing.Size(36, 13);
            this.modelLabel.TabIndex = 1;
            this.modelLabel.Text = "Model";
            // 
            // modelTextBox
            // 
            this.modelTextBox.Location = new System.Drawing.Point(55, 53);
            this.modelTextBox.Name = "modelTextBox";
            this.modelTextBox.Size = new System.Drawing.Size(217, 20);
            this.modelTextBox.TabIndex = 2;
            // 
            // engineGroupBox
            // 
            this.engineGroupBox.Controls.Add(this.engineModelTextBox);
            this.engineGroupBox.Controls.Add(this.engineModelLabel);
            this.engineGroupBox.Controls.Add(this.engineHorsePowerTextBox);
            this.engineGroupBox.Controls.Add(this.engineHorserPowerLabel);
            this.engineGroupBox.Controls.Add(this.engineDisplacementTextBox);
            this.engineGroupBox.Controls.Add(this.engineDisplacementLabel);
            this.engineGroupBox.Location = new System.Drawing.Point(16, 79);
            this.engineGroupBox.Name = "engineGroupBox";
            this.engineGroupBox.Size = new System.Drawing.Size(256, 119);
            this.engineGroupBox.TabIndex = 3;
            this.engineGroupBox.TabStop = false;
            this.engineGroupBox.Text = "Car engine";
            // 
            // engineDisplacementTextBox
            // 
            this.engineDisplacementTextBox.Location = new System.Drawing.Point(95, 25);
            this.engineDisplacementTextBox.Name = "engineDisplacementTextBox";
            this.engineDisplacementTextBox.Size = new System.Drawing.Size(139, 20);
            this.engineDisplacementTextBox.TabIndex = 5;
            // 
            // engineDisplacementLabel
            // 
            this.engineDisplacementLabel.AutoSize = true;
            this.engineDisplacementLabel.Location = new System.Drawing.Point(18, 25);
            this.engineDisplacementLabel.Name = "engineDisplacementLabel";
            this.engineDisplacementLabel.Size = new System.Drawing.Size(71, 13);
            this.engineDisplacementLabel.TabIndex = 4;
            this.engineDisplacementLabel.Text = "Displacement";
            // 
            // engineHorsePowerTextBox
            // 
            this.engineHorsePowerTextBox.Location = new System.Drawing.Point(95, 50);
            this.engineHorsePowerTextBox.Name = "engineHorsePowerTextBox";
            this.engineHorsePowerTextBox.Size = new System.Drawing.Size(139, 20);
            this.engineHorsePowerTextBox.TabIndex = 7;
            // 
            // engineHorserPowerLabel
            // 
            this.engineHorserPowerLabel.AutoSize = true;
            this.engineHorserPowerLabel.Location = new System.Drawing.Point(18, 50);
            this.engineHorserPowerLabel.Name = "engineHorserPowerLabel";
            this.engineHorserPowerLabel.Size = new System.Drawing.Size(67, 13);
            this.engineHorserPowerLabel.TabIndex = 6;
            this.engineHorserPowerLabel.Text = "Horse power";
            // 
            // engineModelTextBox
            // 
            this.engineModelTextBox.Location = new System.Drawing.Point(95, 75);
            this.engineModelTextBox.Name = "engineModelTextBox";
            this.engineModelTextBox.Size = new System.Drawing.Size(139, 20);
            this.engineModelTextBox.TabIndex = 9;
            // 
            // engineModelLabel
            // 
            this.engineModelLabel.AutoSize = true;
            this.engineModelLabel.Location = new System.Drawing.Point(18, 75);
            this.engineModelLabel.Name = "engineModelLabel";
            this.engineModelLabel.Size = new System.Drawing.Size(36, 13);
            this.engineModelLabel.TabIndex = 8;
            this.engineModelLabel.Text = "Model";
            // 
            // yearTextBox
            // 
            this.yearTextBox.Location = new System.Drawing.Point(55, 209);
            this.yearTextBox.Name = "yearTextBox";
            this.yearTextBox.Size = new System.Drawing.Size(217, 20);
            this.yearTextBox.TabIndex = 5;
            // 
            // yearLabel
            // 
            this.yearLabel.AutoSize = true;
            this.yearLabel.Location = new System.Drawing.Point(13, 212);
            this.yearLabel.Name = "yearLabel";
            this.yearLabel.Size = new System.Drawing.Size(29, 13);
            this.yearLabel.TabIndex = 4;
            this.yearLabel.Text = "Year";
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(111, 259);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 23);
            this.addButton.TabIndex = 6;
            this.addButton.Text = "Add";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.AddClick);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(197, 259);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 7;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.CancelClick);
            // 
            // AddForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 294);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.yearTextBox);
            this.Controls.Add(this.yearLabel);
            this.Controls.Add(this.engineGroupBox);
            this.Controls.Add(this.modelTextBox);
            this.Controls.Add(this.modelLabel);
            this.Controls.Add(this.titleLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "AddForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Add";
            this.engineGroupBox.ResumeLayout(false);
            this.engineGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.Label modelLabel;
        private System.Windows.Forms.TextBox modelTextBox;
        private System.Windows.Forms.GroupBox engineGroupBox;
        private System.Windows.Forms.TextBox engineDisplacementTextBox;
        private System.Windows.Forms.Label engineDisplacementLabel;
        private System.Windows.Forms.TextBox engineModelTextBox;
        private System.Windows.Forms.Label engineModelLabel;
        private System.Windows.Forms.TextBox engineHorsePowerTextBox;
        private System.Windows.Forms.Label engineHorserPowerLabel;
        private System.Windows.Forms.TextBox yearTextBox;
        private System.Windows.Forms.Label yearLabel;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button cancelButton;
    }
}