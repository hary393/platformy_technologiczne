﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4
{
    public class CarEngine : IComparable
    {
        public double Displacement { get; set; }
        public double HorsePower { get; set; }
        public string Model { get; set; }

        public CarEngine()
        {

        }

        public CarEngine(double displacement, double horsePower, string model)
        {
            Displacement = displacement;
            HorsePower = horsePower;
            Model = model;
        }

        public override String ToString()
        {
            return Model + " " + Displacement + " (" + HorsePower + " hp)";
        }

        public int CompareTo(object obj)
        {
            CarEngine other = (CarEngine)obj;
            if (obj == null)
                return 1;

            double difference = HorsePower - other.HorsePower;

            if (Math.Abs(difference) < 1e-5)
                return 0;
            else if (difference > 0)
                return 1;
            else
                return -1;
        }
    }
}
