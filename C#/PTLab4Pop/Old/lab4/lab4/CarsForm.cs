﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab4
{
    public partial class CarsForm : Form
    {
        SortableBindingList<Car> carBindingList;
        BindingSource carBindingSource = new BindingSource();

        public CarsForm()
        {
            InitializeComponent();
            List<Car> myCars = new List<Car>()
            {
                new Car("E250", new CarEngine(1.8, 204, "CGI"), 2009),
                new Car("E350", new CarEngine(3.5, 292, "CGI"), 2009),
                new Car("A6", new CarEngine(2.5, 187, "FSI"), 2012),
                new Car("A6", new CarEngine(2.8, 220, "FSI"), 2012),
                new Car("A6", new CarEngine(3.0, 295, "TFSI"), 2012),
                new Car("A6", new CarEngine(2.0, 175, "TDI"), 2011),
                new Car("A6", new CarEngine(3.0, 309, "TDI"), 2011),
                new Car("S6", new CarEngine(4.0, 414, "TFSI"), 2012),
                new Car("S8", new CarEngine(4.0, 513, "TFSI"), 2012)
            };
            carBindingList = new SortableBindingList<Car>(myCars);
            carBindingSource.DataSource = carBindingList;

            carGridView.DataSource = carBindingSource;

            //===== Zadanie 1
            // Linq
            var carQuery =
                from car in
                    (from car in myCars
                    where car.Model.Equals("A6")
                    select new
                    {
                        engineType = car.Engine.Model.Equals("TDI") ? "diesel" : "petrol",
                        hppl = car.Engine.HorsePower / car.Engine.Displacement
                    })
                group car by car.engineType into carGroup
                orderby carGroup.Key
                select new
                {
                    engineType = carGroup.Key,
                    avgHPPL = carGroup.Average(x => x.hppl)
                };

            foreach (var e in carQuery)
                Console.WriteLine(e.engineType + ": " + e.avgHPPL);

            // Method syntax
            var carQuery2 = myCars
                .Where(car => car.Model.Equals("A6"))
                .Select(car => new
                {
                    engineType = car.Engine.Model.Equals("TDI") ? "diesel" : "petrol",
                    hppl = car.Engine.HorsePower / car.Engine.Displacement
                })
                .GroupBy(car => car.engineType)
                .Select(carGroup => new
                {
                    engineType = carGroup.Key,
                    avgHPPL = carGroup.Average(x => x.hppl)
                })
                .OrderBy(car => car.engineType);

            Console.WriteLine();
            foreach (var e in carQuery2)
                Console.WriteLine(e.engineType + ": " + e.avgHPPL);

            //===== Zadanie 2
            Func<Car, Car, int> arg1 = delegate(Car first, Car second)
            {
                return -first.Engine.HorsePower.CompareTo(second.Engine.HorsePower);
            };

            Predicate<Car> arg2 = delegate(Car car)
            {
                return car.Engine.Model.Equals("TDI");
            };

            Action<Car> arg3 = delegate(Car car)
            {
                MessageBox.Show(this, car.ToString(), "Car", MessageBoxButtons.OK, MessageBoxIcon.Information);
            };

            myCars.Sort(new Comparison<Car>(arg1));
            myCars.FindAll(arg2).ForEach(arg3);
        }

        private void carGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void addButton_Click(object sender, EventArgs e)
        {
            AddForm addForm = new AddForm();
            addForm.ShowDialog(this);

            // Check if exited via OK button
            if (addForm.exitViaOk == false)
                return;

            carBindingList.Add(addForm.createdCar);
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            List<Car> selectedCars = new List<Car>();
            for (int i = 0; i < carGridView.SelectedRows.Count; i++)
            {
                selectedCars.Add((Car)carGridView.SelectedRows[i].DataBoundItem);
            }

            foreach (Car car in selectedCars)
            {
                carBindingList.Remove(car);
            }
        }

        private void searchInComboBox_Enter(object sender, EventArgs e)
        {
            searchInComboBox.Items.Clear();

            // Load properties into the ComboBox
            PropertyDescriptorCollection properties = ((ITypedList)carBindingSource).GetItemProperties(null);
            foreach (PropertyDescriptor property in properties)
            {
                searchInComboBox.Items.Insert(0, property.Name);
            }

            // Select the first entry
            if (this.searchInComboBox.Items.Count > 0)
            {
                this.searchInComboBox.SelectedIndex = 0;
            }
        }

        private void findButton_Click(object sender, EventArgs e)
        {
            PropertyDescriptor property = null;
            PropertyDescriptorCollection properties = ((ITypedList)carBindingSource).GetItemProperties(null);
            foreach (PropertyDescriptor prop in properties)
            {
                if (searchInComboBox.Text.Equals(prop.Name))
                {
                    property = prop;
                    break;
                }
            }

            if (property == null)
                return;

            try
            {
                object key = property.Converter.ConvertFrom(searchForTextBox.Text);
                int index = ((IBindingList)carBindingList).Find(property, key);

                if (index != -1)
                {
                    carGridView.ClearSelection();
                    carGridView.Rows[index].Selected = true;
                }
            }
            catch (Exception)
            {}
        }
    }
}
