﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4
{
    public class Car
    {
        public String Model { get; set; }
        public CarEngine Engine { get; set; }
        public int Year { get; set; }

        public Car()
        {

        }

        public Car(string model, CarEngine engine, int year)
        {
            Model = model;
            Engine = engine;
            Year = year;
        }

        public override string ToString()
        {
            return Model + " " + Engine + " " + Year;
        }
    }
}
