﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4
{
    class PropertyComparer<T> : IComparer<T>
    {
        PropertyDescriptor property;
        ListSortDirection direction;

        public PropertyComparer(PropertyDescriptor property, ListSortDirection direction)
        {
            this.property = property;
            this.direction = direction;
        }

        public int Compare(T x, T y)
        {
            object xValue = property.GetValue(x);
            object yValue = property.GetValue(y);

            int compareValue = Comparer.Default.Compare(xValue, yValue);
            if (direction == ListSortDirection.Ascending)
                return compareValue;
            else
                return -compareValue;
        }
    }
}
