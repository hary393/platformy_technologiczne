﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.ComponentModel;
using System.Collections;

namespace WindowsFormsApplication1
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }

    public class Engine : IComparable
    {
        public double Displacement { get; set; }
        public double HorsePower { get; set; }
        public string Model { get; set; }

        public Engine()
        {

        }

        public Engine(double displacement, double horsePower, string model)
        {
            Displacement = displacement;
            HorsePower = horsePower;
            Model = model;
        }

        public override String ToString()
        {
            return Model + " " + Displacement + " (" + HorsePower + " hp)";
        }

        public int CompareTo(object obj)
        {
            Engine other = (Engine)obj;
            if (obj == null)
                return 1;

            double difference = HorsePower - other.HorsePower;

            return (int)difference;
        }
    }

    public class Car
    {
        public String Model { get; set; }
        public Engine Engine { get; set; }
        public int Year { get; set; }

        public Car()
        {

        }

        public Car(string model, Engine engine, int year)
        {
            Model = model;
            Engine = engine;
            Year = year;
        }

        public override string ToString()
        {
            return Model + " " + Engine + " " + Year;
        }
    }

    class PropertyComparer<T> : IComparer<T>
    {
        PropertyDescriptor property;
        ListSortDirection direction;

        public PropertyComparer(PropertyDescriptor property, ListSortDirection direction)
        {
            this.property = property;
            this.direction = direction;
        }

        public int Compare(T x, T y)
        {
            object xValue = property.GetValue(x);
            object yValue = property.GetValue(y);

            int compareValue = Comparer.Default.Compare(xValue, yValue);
            if (direction == ListSortDirection.Ascending)
                return compareValue;
            else
                return -compareValue;
        }
    }



    class MyBindingList<T> : BindingList<T>
    {
        bool isSorted { get; set; }

        public MyBindingList()
        { }

        public MyBindingList(IList<T> list)
            : base(list)
        { }

        protected override bool SupportsSortingCore
        {
            get { return true; }
        }

        protected override void ApplySortCore(PropertyDescriptor property, ListSortDirection direction)
        {
            if (property.PropertyType.GetInterface("IComparable") == null)
                return;

            // Get list to sort
            List<T> items = this.Items as List<T>;

            // Apply and set the sort, if items to sort
            if (items != null)
            {
                PropertyComparer<T> pc = new PropertyComparer<T>(property, direction);
                items.Sort(pc);
                isSorted = true;
            }
            else
            {
                isSorted = false;
            }

            // Let bound controls know they should refresh their views
            this.OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
        }

        protected override bool IsSortedCore
        {
            get { return isSorted; }
        }

        protected override void RemoveSortCore()
        {
            isSorted = false;
        }

        protected override bool SupportsSearchingCore
        {
            get { return true; }
        }

        protected override int FindCore(PropertyDescriptor property, object key)
        {
            // Specify search columns
            if (property == null)
                return -1;

            // Get list to search
            List<T> items = this.Items as List<T>;

            // Traverse list for value
            foreach (T item in items)
            {
                // Test column search value
                string value = property.GetValue(item).ToString();

                // If value is the search value, return the 
                // index of the data item
                if (key.ToString() == value) return IndexOf(item);
            }
            return -1;
        }
    }
}
