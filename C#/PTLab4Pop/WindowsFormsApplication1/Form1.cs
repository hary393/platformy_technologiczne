﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {

        MyBindingList<Car> carBindingList;
        BindingSource carBindingSource = new BindingSource();
        public Form1()
        {
            InitializeComponent();
            
            List<Car> myCars = new List<Car>()
            {
                new Car("E250", new Engine(1.8, 204, "CGI"), 2009),
                new Car("E350", new Engine(3.5, 292, "CGI"), 2009),
                new Car("A6", new Engine(2.5, 187, "FSI"), 2012),
                new Car("A6", new Engine(2.8, 220, "FSI"), 2012),
                new Car("A6", new Engine(3.0, 295, "TFSI"), 2012),
                new Car("A6", new Engine(2.0, 175, "TDI"), 2011),
                new Car("A6", new Engine(3.0, 309, "TDI"), 2011),
                new Car("S6", new Engine(4.0, 414, "TFSI"), 2012),
                new Car("S8", new Engine(4.0, 513, "TFSI"), 2012)
            };
            carBindingList = new MyBindingList<Car>(myCars);
            carBindingSource.DataSource = carBindingList;

            dataGridView1.DataSource = carBindingSource;

            //Query syntax:
            var elements =
                from z in
                    (
                        from r in
                            (from car in myCars
                             where car.Model.Equals("A6")
                             let x = car.Engine.HorsePower / car.Engine.Displacement
                             let y = (car.Engine.Model.Equals("TDI") ? "diesel" : "petrol")
                             select new { engineType = y, HPPL = x })
                        group r by new
                        {
                            Type = r.engineType,
                        } into g
                        select new
                        {
                            engineType = g.Key.Type,
                            avgHPPL = g.Sum(x => x.HPPL / g.Count())
                        })
                orderby z.avgHPPL descending
                select z;


            foreach (var e in elements) MessageBox.Show(e.engineType + ": " + e.avgHPPL);

            //Method syntax:
            var elements2 = myCars.Where(a => a.Model.Equals("A6")).Select(car1 => new
            {
                engineType = (car1.Engine.Model.Equals("TDI") ? "diesel" : "petrol"),
                HPPL = car1.Engine.HorsePower / car1.Engine.Displacement
            }
            ).GroupBy(car => car.engineType).Select(carg => new
            {
                engineType = carg.Key,
                avgHPPL = carg.Average(x => x.HPPL)
            }).OrderByDescending(car => car.avgHPPL);

            foreach (var e in elements2) MessageBox.Show(e.engineType + ": " + e.avgHPPL);





            Func<Car, Car, int> arg1 = delegate(Car car1, Car car2) { return (int)(-1 * car1.Engine.HorsePower - car2.Engine.HorsePower); };
            //myCars.Sort(new Comparison<Car>(arg1));
            Predicate<Car> arg2 = (Car p) => { return p.Engine.Model == "TDI"; };
            
            Action<Car> arg3 = delegate(Car a) { MessageBox.Show(a.Model + " " + a.Engine.Displacement + " " + a.Engine.HorsePower + " " + a.Engine.Model + " " + a.Year); };

            myCars.FindAll(arg2).ForEach(arg3);


        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            PropertyDescriptor property = null;
            PropertyDescriptorCollection properties = ((ITypedList)carBindingSource).GetItemProperties(null);
            foreach (PropertyDescriptor prop in properties)
            {
                if (toolStripComboBox1.Text.Equals(prop.Name))
                {
                    property = prop;
                    break;
                }
            }

            if (property == null)
                return;

            try
            {
                object key = property.Converter.ConvertFrom(toolStripTextBox1.Text);
                int index = ((IBindingList)carBindingList).Find(property, key);

                if (index != -1)
                {
                    dataGridView1.ClearSelection();
                    dataGridView1.Rows[index].Selected = true;
                }
            }
            catch (Exception)
            { }
        }

        private void toolStripComboBox1_Enter(object sender, EventArgs e)
        {
            toolStripComboBox1.Items.Clear();

            // Load properties into the ComboBox
            PropertyDescriptorCollection properties = ((ITypedList)carBindingSource).GetItemProperties(null);
            foreach (PropertyDescriptor property in properties)
            {
                toolStripComboBox1.Items.Insert(0, property.Name);
            }

            // Select the first entry
            if (this.toolStripComboBox1.Items.Count > 0)
            {
                this.toolStripComboBox1.SelectedIndex = 0;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double result = Convert.ToDouble(textBox2.Text.ToString());
            double result2 = Convert.ToDouble(textBox3.Text.ToString());
            int result3 = Convert.ToInt32(textBox5.Text.ToString());
            Car newcar = new Car(textBox1.Text.ToString(), new Engine(result,result2 , textBox4.Text.ToString()), result3);
            carBindingList.Add(newcar);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            List<Car> selectedCars = new List<Car>();
            for (int i = 0; i < dataGridView1.SelectedRows.Count; i++)
            {
                selectedCars.Add((Car)dataGridView1.SelectedRows[i].DataBoundItem);
            }

            foreach (Car car in selectedCars)
            {
                carBindingList.Remove(car);
            }
        }
    }
}
