﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {

        string
[] hostNames = { 
"www.microsoft.com", "www.apple.com", "www.google.com", "www.ibm.com", "cisco.netacad.net", "www.oracle.com"
, "www.nokia.com", "www.hp.com", "www.dell.com", "www.samsung.com", "www.toshiba.com"
, "www.siemens.com", "www.amazon.com", "www.sony.com", "www.canon.com", 
"www.alcatel-lucent.com", "www.acer.com", "www.motorola.com"};
        int highestPercentageReached = 0;
        int numberToCompute = 0;
        Random rnd = new Random();
        public Form1()
        {
            InitializeComponent();
            this.backgroundWorker1.WorkerReportsProgress = true;
        }

        private void Rand_Click(object sender, EventArgs e)
        {
            int rand = rnd.Next(1, 999999);
            label12.Text = rand.ToString();
        }

        private void Task_Click(object sender, EventArgs e)
        {
            int N = int.Parse(textBox1.Text.ToString());
            int K = int.Parse(textBox2.Text.ToString());

            Task t = Task.Factory.StartNew(() =>
            {
                Task<double> licz = Task<double>.Factory.StartNew(() =>
                {
                    double licznik = 1;
                    for (int x = N; x > N - K; x--)
                    {
                        licznik *= x;
                    }
                    Thread.Sleep(3000);
                    return licznik;
                });

                double mianownik = 1;
                for (int x = 1; x < K + 1; x++)
                {
                    mianownik *= x;
                }
                licz.Wait();
                double wynik = licz.Result / mianownik;

                this.Invoke((MethodInvoker)delegate
                {
                    label3.Text = wynik.ToString();
                });



            });
        }
        private int Mian(int K)
        {
            int mianownik = 1;
            for (int x = 1; x < K + 1; x++)
            {
                mianownik *= x;
            }
            return mianownik;
        }
        private delegate int MianDele(int K);
        private int Licz(int K,int N)
        {
            int licznik = 1;
            for (int x = N; x > N - K; x--)
            {
                licznik *= x;
            }
            Thread.Sleep(3000);
            return licznik;
        }
        private delegate int LiczDele(int K, int N);


        public IAsyncResult BeginPrzeniesPliki
      (int K, int N, AsyncCallback ac, Object stan)

        {
            MianDele d = new MianDele(Mian);
            LiczDele l = new LiczDele(Licz);
            IAsyncResult wynik = d.BeginInvoke(K,ac,stan);
            IAsyncResult wynik2 = l.BeginInvoke(K,N, ac, stan);
            int end = l.EndInvoke(wynik2) / d.EndInvoke(wynik);
            this.Invoke((MethodInvoker)delegate
            {
                label4.Text = end.ToString();
            });
            return wynik;

        }

        private void Delegates_Click(object sender, EventArgs e)
        {
            int N = Convert.ToInt32(textBox1.Text.ToString());
            int K = Convert.ToInt32(textBox2.Text.ToString());
            Task t = Task.Factory.StartNew(() =>
            {
                BeginPrzeniesPliki(K,N,null,null);
            });
        }

        private void Async_Click(object sender, EventArgs e)
        {
            int N = Convert.ToInt32(textBox1.Text.ToString());
            int K = Convert.ToInt32(textBox2.Text.ToString());
            Task t = Task.Factory.StartNew(async () =>
            {
                Task<int> access = DoSomethingAsync(K);
                Task<int> access2 = DoSomethingAsync2(K, N);
                int x = await access;
                int y = await access2;
                double wynik = y / x;

                this.Invoke((MethodInvoker)delegate
                {
                    label5.Text = wynik.ToString();
                });
            });

        }
        async Task<int> DoSomethingAsync(int K)
        {
            await Task.Delay(10);
            int mianownik = 1;
            for (int x = 1; x < K + 1; x++)
            {
                mianownik *= x;
            }
            return mianownik;
        }
        async Task<int> DoSomethingAsync2(int K,int N)
        {
            await Task.Delay(10);
            int licznik = 1;
            for (int x = N; x > N - K; x--)
            {
                licznik *= x;
            }
            Thread.Sleep(3000);
            return licznik;
        }
        private void Fibbonaci_Click(object sender, EventArgs e)
        {
            int I = Convert.ToInt32(textBox3.Text.ToString());
            Task t = Task.Factory.StartNew(() =>
            {

                backgroundWorker1.DoWork +=
                new DoWorkEventHandler(backgroundWorker1_DoWork);
                backgroundWorker1.RunWorkerCompleted +=
                    new RunWorkerCompletedEventHandler(
                backgroundWorker1_RunWorkerCompleted);
                backgroundWorker1.ProgressChanged +=
                    new ProgressChangedEventHandler(
                backgroundWorker1_ProgressChanged);


                numberToCompute = I;

                // Reset the variable for percentage tracking.
                highestPercentageReached = 0;

                // Start the asynchronous operation.
                backgroundWorker1.RunWorkerAsync(numberToCompute);



            });
        }
        private void backgroundWorker1_DoWork(object sender,
            DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            e.Result = ComputeFibonacci((int)e.Argument, worker, e);
        }

        private void backgroundWorker1_RunWorkerCompleted(
            object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message);
            }
            else
            {
                this.Invoke((MethodInvoker)delegate
                {
                    label8.Text = e.Result.ToString();
                });
                
            }
        }
        private void backgroundWorker1_ProgressChanged(object sender,
            ProgressChangedEventArgs e)
        {
            this.Invoke((MethodInvoker)delegate
            {
                this.progressBar1.Value = e.ProgressPercentage;
            });
            
        }

        long ComputeFibonacci(int n, BackgroundWorker worker, DoWorkEventArgs e)
        {
            // The parameter n must be >= 0 and <= 91.
            // Fib(n), with n > 91, overflows a long.
            if ((n < 0) || (n > 91))
            {
                throw new ArgumentException(
                    "value must be >= 0 and <= 91", "n");
            }

            long result = 0;
                if (n < 2)
                {
                    result = 1;
                }
                else
                {
                    result = ComputeFibonacci(n - 1, worker, e) +
                             ComputeFibonacci(n - 2, worker, e);
                    Task.Delay(300);
                 }

                // Report progress as a percentage of the total task.
                int percentComplete =
                    (int)((float)n / (float)numberToCompute * 100);
                if (percentComplete > highestPercentageReached)
                {
                    highestPercentageReached = percentComplete;
                
                    worker.ReportProgress(percentComplete);
                }
            return result;
        }
        private void Compress_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.ShowDialog();
            DirectoryInfo directorySelected = new DirectoryInfo(folderBrowserDialog1.SelectedPath);
            FileInfo[] f = directorySelected.GetFiles();
            Parallel.ForEach(f, (currentfile) =>
            {
                using (FileStream originalFileStream = currentfile.OpenRead())
                {
                    if ((File.GetAttributes(currentfile.FullName) &
                       FileAttributes.Hidden) != FileAttributes.Hidden & currentfile.Extension != ".gz")
                    {
                        using (FileStream compressedFileStream = File.Create(currentfile.FullName + ".gz"))
                        {
                            using (GZipStream compressionStream = new GZipStream(compressedFileStream,
                               CompressionMode.Compress))
                            {
                                originalFileStream.CopyTo(compressionStream);

                            }
                        }
                        FileInfo info = new FileInfo(folderBrowserDialog1.SelectedPath + "\\" + currentfile.Name + ".gz");
                        Console.WriteLine("Compressed {0} from {1} to {2} bytes.",
                        currentfile.Name, currentfile.Length.ToString(), info.Length.ToString());
                    }

                }
            });
        }

        private void Decompress_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.ShowDialog();
            DirectoryInfo directorySelected = new DirectoryInfo(folderBrowserDialog1.SelectedPath);
            FileInfo[] f = directorySelected.GetFiles();
            Parallel.ForEach(f, (currentfile) =>
            {
                using (FileStream originalFileStream = currentfile.OpenRead())
                {
                    string currentFileName = currentfile.FullName;
                    string newFileName = currentFileName.Remove(currentFileName.Length - currentfile.Extension.Length);

                    using (FileStream decompressedFileStream = File.Create(newFileName))
                    {
                        using (GZipStream decompressionStream = new GZipStream(originalFileStream, CompressionMode.Decompress))
                        {
                            decompressionStream.CopyTo(decompressedFileStream);
                            Console.WriteLine("Decompressed: {0}", currentfile.Name);
                        }
                    }
                }
            });
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Thread hue = new Thread(DNS);
            hue.Start();
        }
        void DNS(){
            string tekst = "";
            hostNames.AsParallel().ForAll(en =>
            {
                string ciąg = "\n" + en.ToString();
                ciąg += " => ";
                try
                {
                    ciąg += Dns.GetHostAddresses(en.ToString())[0];
                }
                catch (Exception exp)
                {
                }
                tekst += ciąg;


            });
            this.Invoke((MethodInvoker)delegate
            {
                richTextBox1.Text = tekst;
            });
        }

    }
}
