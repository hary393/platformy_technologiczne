﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace LabPT1
{
    class Program
    {
        [Serializable]
        private class StringCmp : IComparer<String>
        {
           public int Compare(String a, String b)
            {
                if (a.Length == b.Length)
                {
                    return a.CompareTo(b);
                }
                if (a.Length < b.Length)
                    return -1;
                else
                    return 1;
                return 0;
            }
        }



        static void DirSearch(string dir,int poziom)
        {
            try
            {
                DirectoryInfo hue = new DirectoryInfo(dir);
                FileSystemInfo[] files = hue.GetFiles();
                DirectoryInfo[] dirs = hue.GetDirectories();
                foreach (var d in dirs)
                {
                    FileSystemInfo[] files2 = d.GetFiles();
                    DirectoryInfo[] dirs2 = d.GetDirectories();
                    int rozm=files2.Length+dirs2.Length;
                    for (int z = 0; z < poziom;z++ )
                        Console.Write("\t");
                    Console.Write(d.Name + " (" + rozm + ")");
                    d.Atrybuty();
                    DirSearch(d.FullName,poziom+1);
                }
                foreach (FileInfo kek in files)
                {
                    for (int z = 0; z < poziom; z++)
                        Console.Write("\t");
                    kek.Atrybuty();
                }
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        static void Main(string[] args)
        {

            DirectoryInfo hue = new DirectoryInfo("C:\\Users\\student\\Pictures");
            FileSystemInfo[] files = hue.GetFiles();
            DirectoryInfo[] dirs = hue.GetDirectories();
            int rozm=files.Length+dirs.Length;
            Console.Write(hue.Name + " (" + rozm + ")");
            hue.Atrybuty();
            DirSearch("C:\\Users\\student\\Pictures",1);
            hue.Data();
            StringCmp cmp = new StringCmp();
            SortedDictionary<String,long> dict=new SortedDictionary<String,long>(cmp);
            SortedDictionary<String, long> dict2 = new SortedDictionary<String, long>(cmp);
            foreach (FileInfo kek in files)
            {
                dict.Add(kek.Name, kek.Length);
            }
            foreach (var d in dirs)
            {
                FileSystemInfo[] files2 = d.GetFiles();
                DirectoryInfo[] dirs2 = d.GetDirectories();
                long rozm2 = files2.Length + dirs2.Length;
                dict.Add(d.Name, rozm2);
            }
            BinaryFormatter ser = new BinaryFormatter();
            FileStream fs = new FileStream("DataFile.dat", FileMode.Create);
            ser.Serialize(fs, dict);
            fs.Close();
            FileStream fs2 = new FileStream("DataFile.dat", FileMode.Open);
            dict2 = (SortedDictionary<String,long>)ser.Deserialize(fs2);


            foreach (var p in dict2)
            {
                Console.WriteLine(p.Key+" -> "+p.Value);
            }


        }
    }

    static class Rozszerzenia
    {
        public static void Atrybuty(this FileInfo str)
        {
            string atr = "";
            Console.Write(str.ToString() + " " + str.Length + "b ");
            string lel = str.Attributes.ToString();
            if (lel.Contains("Read Only"))
                atr += "r";
            else
                atr += "-";
            if (lel.Contains("Archive"))
                atr += "a";
            else
                atr += "-";
            if (lel.Contains("System"))
                atr += "s";
            else
                atr += "-";
            if (lel.Contains("Hidden"))
                atr += "h";
            else
                atr += "-";
            Console.WriteLine(atr);
        }
        public static void Atrybuty(this DirectoryInfo str)
        {
            string atr = "";
            string lel = str.Attributes.ToString();
            if (lel.Contains("Read Only"))
                atr += "r";
            else
                atr += "-";
            if (lel.Contains("Archive"))
                atr += "a";
            else
                atr += "-";
            if (lel.Contains("System"))
                atr += "s";
            else
                atr += "-";
            if (lel.Contains("Hidden"))
                atr += "h";
            else
                atr += "-";
            Console.WriteLine(atr);
        }

        public static void Data(this DirectoryInfo hue)
        {
            DateTime czasbest;
            if (hue.Exists)
            {
                FileSystemInfo[] files = hue.GetFiles("*.*", SearchOption.AllDirectories);
                czasbest = files[0].CreationTime;
                foreach (FileInfo kek in files)
                {
                    if (kek.CreationTime < czasbest)
                    {
                        czasbest = kek.CreationTime;
                    }
                }
                Console.WriteLine("najstarszy plik "+czasbest);
            }
        }
    
    }
}
